# DecaWave Localization ROS Node
This ROS node for localization using a set of the DecaWave TREK1000.
This ROS node uses known anchor positions to solve for the position of a tag. This approach uses [Equation of a Circle from 3 Points (2 dimensions)
](http://paulbourke.net/geometry/circlesphere/) to determine the position of the tag. 

# Running
1. Turn on anchor 0 and plug it into the computer you want get position data, and turn on other two anchors and one tag.
2. Record the locations of the anchors relative to a known origin in
`nodes/tag.py`
3. Run `rosrun evk1000_localization tag.py` with the appropriate
parameters for your current setup
4. Run `rostopic echo /tag_location` to see the current pose of
your tag
