#!/usr/bin/env python

import rosbag
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.optimize import curve_fit
from scipy import optimize


##### Load the bag file #####
# bag = rosbag.Bag('gem_track.bag')
bag = rosbag.Bag('2019-09-04-19-45-55.bag')
gem_pos = []
for topic, msg, t in bag.read_messages(topics=['/gem_location']):
	if msg.data != (999., 999.):
		gem_pos.append(msg.data)
		# print [t, msg.data] # [time_stamp, (x, y)]
		print[t, msg.time, msg.data]
bag.close()

gem_pos = np.vstack(gem_pos)

xdata = gem_pos[:, 0].reshape(gem_pos.shape[0])
ydata = gem_pos[:, 1].reshape(gem_pos.shape[0])


##### Set up the figure ######


fig = plt.figure()
ax = plt.axes(xlim=(0, 15), ylim=(-15, 0))



# plt.plot(gem_pos[:, 0], gem_pos[:, 1], 'k*', markersize=5.)

# axes = plt.gca()
# axes.set_xlim([0, 15])
# axes.set_ylim([-15, 0])
plt.gca().set_aspect('equal', adjustable='box')



##### Fit the circle #####

method_2 = "leastsq"
x = xdata
y = ydata

def calc_R(xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return np.sqrt((x-xc)**2 + (y-yc)**2)

def f_2(c):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(*c)
    return Ri - Ri.mean()

x_m = 7.
y_m = -7.
center_estimate = x_m, y_m
center_2, ier = optimize.leastsq(f_2, center_estimate)

xc_2, yc_2 = center_2
Ri_2       = calc_R(*center_2)
R_2        = Ri_2.mean()
residu_2   = sum((Ri_2 - R_2)**2)

x_fit = []
y_fit = []
# xc = 7.
# yc = -7.
# R = 6.
for theta in np.arange(0, 2.*np.pi, 0.01):
	x_fit.append(xc_2 + R_2 * np.cos(theta))
	y_fit.append(yc_2 + R_2 * np.sin(theta))



xc = xc_2
yc = yc_2
Rc = R_2

print('xc: {}, yc: {}, Rc: {}'.format(xc, yc, Rc))

gem_pos_proj = []
gem_orientation = []
for i in range(xdata.shape[0]):
	drct = gem_pos[i, :] - np.array([xc, yc])
	drct = drct / np.linalg.norm(drct)
	gem_pos_proj.append(np.array([xc, yc]) + drct * Rc)
	drct_tan = np.array([drct[1], -drct[0]])
	gem_orientation.append(drct_tan)
	

gem_pos_proj = np.vstack(gem_pos_proj)
# plt.plot(gem_pos_proj[:, 0], gem_pos_proj[:, 1], 'b^', markersize=10.)
gem_orientation = np.vstack(gem_orientation)



for i in range(xdata.shape[0]):
	xx = np.linspace(gem_pos_proj[i, 0], gem_pos_proj[i, 0] + gem_orientation[i, 0] * 10., num=100)
	yy = np.linspace(gem_pos_proj[i, 1], gem_pos_proj[i, 1] + gem_orientation[i, 1] * 10., num=100)
	# plt.plot(xx, yy, 'r-')
	


# line, = ax.plot([], [], lw=2)
# plt.plot(gem_pos[:, 0], gem_pos[:, 1], 'k*', markersize=5.)

gem_pos_anim, = ax.plot([], [], 'k*', markersize = 5.)
gem_pos_proj_anim, = ax.plot([], [], 'b^', markersize=5.)
gem_orientation_anim, = ax.plot([], [], 'r-', linewidth = 2.)



def init():
	# plt.plot(x_fit, y_fit, 'b-') # draw the circle
	return

def animate(i):
	gem_pos_anim.set_data(gem_pos[:i+1, 0], gem_pos[:i+1, 1])
	gem_pos_proj_anim.set_data(gem_pos_proj[:i+1, 0], gem_pos_proj[:i+1, 1])
	xx = np.linspace(gem_pos_proj[i, 0], gem_pos_proj[i, 0] + gem_orientation[i, 0] * 3., num=100)
	yy = np.linspace(gem_pos_proj[i, 1], gem_pos_proj[i, 1] + gem_orientation[i, 1] * 3., num=100)
	gem_orientation_anim.set_data(xx, yy)
	
def animate_orientation_on_true_pos(i):
	gem_pos_anim.set_data(gem_pos[:i+1, 0], gem_pos[:i+1, 1])
	# gem_pos_proj_anim.set_data(gem_pos_proj[:i+1, 0], gem_pos_proj[:i+1, 1])
	xx = np.linspace(gem_pos[i, 0], gem_pos[i, 0] + gem_orientation[i, 0] * 3., num=100)
	yy = np.linspace(gem_pos[i, 1], gem_pos[i, 1] + gem_orientation[i, 1] * 3., num=100)
	gem_orientation_anim.set_data(xx, yy)


anim = animation.FuncAnimation(fig, animate_orientation_on_true_pos, init_func=init,\
	frames=xdata.shape[0], interval=200)


# x_anim = X[0, :]
# y_anim = X[1, :]

# ped_track, = axs[0].plot([], [], '*', ms=5)
# prob_dist = axs[1].bar([1,2,3],[1./3, 1./3, 1./3])
# first_plot_num_points = params.init_point_index + 1
# # first_plot_num_points = 0 + 1 = 1

# def init():

#     ped_track.set_data(x_anim[:first_plot_num_points], \
#     	y_anim[:first_plot_num_points]) # plot first few points. (now it is only 1)
#     for j, b in enumerate(prob_dist): # j is 0, 1, 2
#     	b.set_height(1./3)

# def animate(i): # i starts from 0. is frames=50, last i would be 49.

# 	# print(i) # in case you want to debug the index
#     x = x_anim[:i+first_plot_num_points+1] # start from plotting 16 points
#     y = y_anim[:i+first_plot_num_points+1]
#     ped_track.set_data(x, y)
#     for j, b in enumerate(prob_dist): # k is 0, 1, 2
#     	b.set_height(weightOnIntent_hist[i, j])

# anim = animation.FuncAnimation(fig, animate, init_func=init,\
# 	frames=x_anim.shape[0]-first_plot_num_points, interval=params.anim_interval)






















#***************************#
# fig, axs = plt.subplots(1,2,figsize=(18,7)) # axs.shape = (2, )

# # plot intents
# intent_num = 3
# intent_color = 'ryg' # red, yellow, green
# intent_theta = np.arange(0, 2. * np.pi + np.pi / 50., np.pi / 50.)
# for i in range(intent_num):
# 	intent_x = intent_radii[i, 0] * np.cos(intent_theta) + intent_mean[i, 0]
# 	intent_y = intent_radii[i, 0] * np.sin(intent_theta) + intent_mean[i, 1]
# 	axs[0].plot(intent_x, intent_y, color=intent_color[i], linestyle='-', linewidth=2.)

# # plot walls
# axs[0].plot(wall_sources[:, 0], wall_sources[:, 1], 'k*', markersize=5.)
# axs[0].axis('scaled') # amazing solution
# axs[0].set_xlim([-1, 17])
# axs[0].set_ylim([-1, 13])
# axs[1].set_ylim([0, 1])


# ########## Animation ##########

# x_anim = X[0, :]
# y_anim = X[1, :]
# ped_track, = axs[0].plot([], [], '*', ms=5)
# prob_dist = axs[1].bar([1,2,3],[1./3, 1./3, 1./3])
# first_plot_num_points = params.init_point_index + 1
# # first_plot_num_points = 0 + 1 = 1

# def init():

#     ped_track.set_data(x_anim[:first_plot_num_points], \
#     	y_anim[:first_plot_num_points]) # plot first few points. (now it is only 1)
#     for j, b in enumerate(prob_dist): # j is 0, 1, 2
#     	b.set_height(1./3)

# def animate(i): # i starts from 0. is frames=50, last i would be 49.

# 	# print(i) # in case you want to debug the index
#     x = x_anim[:i+first_plot_num_points+1] # start from plotting 16 points
#     y = y_anim[:i+first_plot_num_points+1]
#     ped_track.set_data(x, y)
#     for j, b in enumerate(prob_dist): # k is 0, 1, 2
#     	b.set_height(weightOnIntent_hist[i, j])

# anim = animation.FuncAnimation(fig, animate, init_func=init,\
# 	frames=x_anim.shape[0]-first_plot_num_points, interval=params.anim_interval)



plt.show()

