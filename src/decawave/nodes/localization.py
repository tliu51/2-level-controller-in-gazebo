#!/usr/bin/env python
# Authors: Haonan Chen, Zhe Huang.
# Date: Aug. 30, 2019

import math
import rospy
import serial
# from std_msgs.msg import Float64MultiArray
import numpy as np
import time
from decawave.msg import position

NODE_NAME = "gem_localization"

# EPSILON is a really small number to adjust precision
EPSILON = 1.5
# Initial position for three anchors
X0, Y0 = 8.65, 0.63
X1, Y1 = 0., -16.
X2, Y2 = 12.65, -15.

class DecaWaveTag:

    def __init__(self):
        # port and baud are port and band rate that could get raw data 
        port = rospy.get_param('~port', '/dev/ttyACM0')
        baud = rospy.get_param('~baud', 115200)

        self.rate = rospy.Rate(rospy.get_param("frequency", 100))
        self.ser = serial.Serial(port=port, timeout=None, baudrate=baud)
        self.x0, self.y0 = X0, Y0
        self.x1, self.y1 = X1, Y1
        self.x2, self.y2 = X2, Y2

    def get_dist(self):
        '''
            input: nothing
            output: a list of distances between tag and three anchors
        '''
        raw_data = self.ser.readline() #Note: the number of character of one line is 63 
        data = raw_data.split()

        while len(data) > 0 :
            if data[0] == 'mc':     # mc indicates the distance between different anchors and tag 
                dist_t_a0 = int(data[2], 16) / 1000.0
                dist_t_a1 = int(data[3], 16) / 1000.0
                dist_t_a2 = int(data[4], 16) / 1000.0
                break 
            raw_data = self.ser.readline()
            data = raw_data.split()
        return [dist_t_a0, dist_t_a1, dist_t_a2]

    def run(self):
        '''
            input: nothing
            output: this function publish location of the tag as a ROS topic
        '''
        self.loc_pub = rospy.Publisher('gem_location', position)

        while not rospy.is_shutdown():
            dist = self.get_dist()     
            results = []
            results.append(self.calculateThreeCircleIntersection(self.x0, self.y0, dist[0], self.x1, self.y1, dist[1], self.x2, self.y2, dist[2]))
            results.append(self.calculateThreeCircleIntersection(self.x1, self.y1, dist[1], self.x2, self.y2, dist[2], self.x0, self.y0, dist[0]))
            results.append(self.calculateThreeCircleIntersection(self.x2, self.y2, dist[2], self.x0, self.y0, dist[0], self.x1, self.y1, dist[1]))       
            coord_accum = np.array([0., 0.])
            iter_accum = 0.
            
            for i in range(3):
                if type(results[i]) != bool:
                    coord_accum = coord_accum + results[i]
                    iter_accum = iter_accum + 1.

            if iter_accum > 0:
                [x, y] = coord_accum / iter_accum
            else:
                [x, y] = [999., 999.]
            #Float64MultiArray is the type of ROS topic that the node will publish
            # my_array_for_publishing = Float64MultiArray(data=[x,y])
            my_array_for_publishing = position(time=int(round(time.time() * 1000)),\
             data=[x,y])
            rospy.loginfo(my_array_for_publishing)
            self.loc_pub.publish(my_array_for_publishing)

            self.rate.sleep()


    def calculateThreeCircleIntersection(self, x0, y0, r0, x1, y1, r1, x2, y2, r2):
        '''
            input: three anchors location and distance between anchors and tag  
                    (x0,y0)- anchor 0 location
                    (x1,y1)- anchor 1 location
                    (x2,y2)- anchor 2 location
                    r0 - distance between anchor 0 and tag
                    r1 - distance between anchor 1 and tag
                    r2 - distance between anchor 2 and tag
            output: this function calculate the location of the tag
            explaination: http://paulbourke.net/geometry/circlesphere/
        '''
        #dx and dy are the vertical and horizontal distances between the circle centers.
        dx = x1 - x0
        dy = y1 - y0

        #Determine the straight-line distance between the centers
        d = (dx**2 + dy**2)**0.5

        #Check for solvability.
        if (d > (r0 + r1)):
            #no solution. circles do not intersect. 
            print("Some of anchors are off!")
            return False
        if (d < abs(r0 - r1)):
            print("Some of anchors are off!")
            return False

        # 'point 2' is the point where the line through the circle intersection points crosses the line between the circle centers.

        # Determine the distance from point 0 to point 2.
        a = ((r0*r0) - (r1*r1) + (d*d)) / (2.0 * d) # proved correct by zhe

        # Determine the coordinates of point 2.
        point2_x = x0 + (dx * a/d)
        point2_y = y0 + (dy * a/d)

        # Determine the distance from point 2 to either of the intersection points.
        h = ((r0*r0) - (a*a))**0.5

        #Now determine the offsets of the intersection points from point 2.
        rx = -dy * (h/d)
        ry = dx * (h/d)

        # Determine the absolute intersection points. 
        intersectionPoint1_x = point2_x + rx
        intersectionPoint2_x = point2_x - rx
        intersectionPoint1_y = point2_y + ry
        intersectionPoint2_y = point2_y - ry

        # Lets determine if circle 3 intersects at either of the above intersection points. 
        dx = intersectionPoint1_x - x2
        dy = intersectionPoint1_y - y2
        d1 = ((dy*dy) + (dx*dx))**0.5

        dx = intersectionPoint2_x - x2
        dy = intersectionPoint2_y - y2
        d2 = ((dy*dy) + (dx*dx))**0.5
        if(abs(d1 - r2) < EPSILON):            
            return[intersectionPoint1_x, intersectionPoint1_y]

        elif(abs(d2 - r2) < EPSILON):
            return [intersectionPoint2_x, intersectionPoint2_y]
        else:
            print("Some of anchors are off!")
            return False





if __name__ == "__main__":
    rospy.init_node(NODE_NAME, anonymous=True)
    da = DecaWaveTag()
    try:
        da.run()
    except rospy.ROSInterruptException:
        pass

    
