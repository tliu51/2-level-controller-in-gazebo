#!/usr/bin/env python
# import rosbag
# # bag = rosbag.Bag('data.bag')
# bag = rosbag.Bag('gem_track.bag')
# for topic, msg, t in bag.read_messages(topics=['/gem_location']):
# 	print [t, msg.data] # [time_stamp, (x, y)]
# bag.close()


import rosbag
import numpy as np



# bag = rosbag.Bag('gem_track.bag')
bag = rosbag.Bag('2019-09-04-19-45-55.bag')
gem_pos = []
for topic, msg, t in bag.read_messages(topics=['/gem_location']):
	if msg.data != (999., 999.):
		gem_pos.append(msg.data)
		# print [t, msg.data] # [time_stamp, (x, y)]
		print[t, msg.time, msg.data]
bag.close()

gem_pos = np.vstack(gem_pos)



# import scipy.io as sio
# import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.animation as animation
from scipy.optimize import curve_fit


xdata = gem_pos[:, 0].reshape(gem_pos.shape[0])
ydata = gem_pos[:, 1].reshape(gem_pos.shape[0])

# fig = plt.figure(figsize = (10, 10))
fig = plt.figure()

# def func(x, a, b, c):
# 	# return a * x ** 2. + b * x + c
# 	return 
# 	# return a * np.exp(-b * x) + c

# def func(x, x0, y0, r):
# 	return y0 - np.sqrt(r**2. - (x-x0)**2.)
# 	# return a * np.exp(-b * x) + c

def func(x, a, b, c, d):
	return a * x ** 3. + b * x ** 2. + c * x + d
	# return a * np.exp(-b * x) + c

popt, pcov = curve_fit(func, xdata, ydata)
# popt
# array([ 2.55423706,  1.35190947,  0.47450618])
# plt.plot(xdata, func(xdata, *popt), 'r-')



plt.plot(gem_pos[:, 0], gem_pos[:, 1], 'k*', markersize=5.)

axes = plt.gca()
axes.set_xlim([0, 15])
axes.set_ylim([-15, 0])
plt.gca().set_aspect('equal', adjustable='box')



# #! python
# # == METHOD 1 ==
# method_1 = 'algebraic'

# x = xdata
# y = ydata
# # coordinates of the barycenter
# x_m = mean(x)
# y_m = mean(y)

# # calculation of the reduced coordinates
# u = x - x_m
# v = y - y_m

# # linear system defining the center (uc, vc) in reduced coordinates:
# #    Suu * uc +  Suv * vc = (Suuu + Suvv)/2
# #    Suv * uc +  Svv * vc = (Suuv + Svvv)/2
# Suv  = sum(u*v)
# Suu  = sum(u**2)
# Svv  = sum(v**2)
# Suuv = sum(u**2 * v)
# Suvv = sum(u * v**2)
# Suuu = sum(u**3)
# Svvv = sum(v**3)

# # Solving the linear system
# A = array([ [ Suu, Suv ], [Suv, Svv]])
# B = array([ Suuu + Suvv, Svvv + Suuv ])/2.0
# uc, vc = linalg.solve(A, B)

# xc_1 = x_m + uc
# yc_1 = y_m + vc

# # Calcul des distances au centre (xc_1, yc_1)
# Ri_1     = sqrt((x-xc_1)**2 + (y-yc_1)**2)
# R_1      = mean(Ri_1)
# residu_1 = sum((Ri_1-R_1)**2)

#***************************#

from scipy import optimize

method_2 = "leastsq"
x = xdata
y = ydata

def calc_R(xc, yc):
    """ calculate the distance of each 2D points from the center (xc, yc) """
    return np.sqrt((x-xc)**2 + (y-yc)**2)

def f_2(c):
    """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
    Ri = calc_R(*c)
    return Ri - Ri.mean()

x_m = 7.
y_m = -7.
center_estimate = x_m, y_m
center_2, ier = optimize.leastsq(f_2, center_estimate)

xc_2, yc_2 = center_2
Ri_2       = calc_R(*center_2)
R_2        = Ri_2.mean()
residu_2   = sum((Ri_2 - R_2)**2)

x_fit = []
y_fit = []
# xc = 7.
# yc = -7.
# R = 6.
for theta in np.arange(0, 2.*np.pi, 0.01):
	x_fit.append(xc_2 + R_2 * np.cos(theta))
	y_fit.append(yc_2 + R_2 * np.sin(theta))

plt.plot(x_fit, y_fit, 'b-')

xc = xc_2
yc = yc_2
Rc = R_2

print('xc: {}, yc: {}, Rc: {}'.format(xc, yc, Rc))

gem_pos_proj = []
gem_orientation = []
for i in range(xdata.shape[0]):
	drct = gem_pos[i, :] - np.array([xc, yc])
	drct = drct / np.linalg.norm(drct)
	gem_pos_proj.append(np.array([xc, yc]) + drct * Rc)
	drct_tan = np.array([drct[1], -drct[0]])
	gem_orientation.append(drct_tan)
	

gem_pos_proj = np.vstack(gem_pos_proj)
plt.plot(gem_pos_proj[:, 0], gem_pos_proj[:, 1], 'b^', markersize=10.)
gem_orientation = np.vstack(gem_orientation)



for i in range(xdata.shape[0]):
	xx = np.linspace(gem_pos_proj[i, 0], gem_pos_proj[i, 0] + gem_orientation[i, 0] * 10., num=100)
	yy = np.linspace(gem_pos_proj[i, 1], gem_pos_proj[i, 1] + gem_orientation[i, 1] * 10., num=100)
	plt.plot(xx, yy, 'r-')
	

# plt.show()





























#***************************#
# fig, axs = plt.subplots(1,2,figsize=(18,7)) # axs.shape = (2, )

# # plot intents
# intent_num = 3
# intent_color = 'ryg' # red, yellow, green
# intent_theta = np.arange(0, 2. * np.pi + np.pi / 50., np.pi / 50.)
# for i in range(intent_num):
# 	intent_x = intent_radii[i, 0] * np.cos(intent_theta) + intent_mean[i, 0]
# 	intent_y = intent_radii[i, 0] * np.sin(intent_theta) + intent_mean[i, 1]
# 	axs[0].plot(intent_x, intent_y, color=intent_color[i], linestyle='-', linewidth=2.)

# # plot walls
# axs[0].plot(wall_sources[:, 0], wall_sources[:, 1], 'k*', markersize=5.)
# axs[0].axis('scaled') # amazing solution
# axs[0].set_xlim([-1, 17])
# axs[0].set_ylim([-1, 13])
# axs[1].set_ylim([0, 1])


# ########## Animation ##########

# x_anim = X[0, :]
# y_anim = X[1, :]
# ped_track, = axs[0].plot([], [], '*', ms=5)
# prob_dist = axs[1].bar([1,2,3],[1./3, 1./3, 1./3])
# first_plot_num_points = params.init_point_index + 1
# # first_plot_num_points = 0 + 1 = 1

# def init():

#     ped_track.set_data(x_anim[:first_plot_num_points], \
#     	y_anim[:first_plot_num_points]) # plot first few points. (now it is only 1)
#     for j, b in enumerate(prob_dist): # j is 0, 1, 2
#     	b.set_height(1./3)

# def animate(i): # i starts from 0. is frames=50, last i would be 49.

# 	# print(i) # in case you want to debug the index
#     x = x_anim[:i+first_plot_num_points+1] # start from plotting 16 points
#     y = y_anim[:i+first_plot_num_points+1]
#     ped_track.set_data(x, y)
#     for j, b in enumerate(prob_dist): # k is 0, 1, 2
#     	b.set_height(weightOnIntent_hist[i, j])

# anim = animation.FuncAnimation(fig, animate, init_func=init,\
# 	frames=x_anim.shape[0]-first_plot_num_points, interval=params.anim_interval)

plt.show()
