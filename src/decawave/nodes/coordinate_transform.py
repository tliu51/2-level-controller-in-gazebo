#!/usr/bin/env python

# coordinate_transform.py
# ver 0.2
# Author: Zhe Huang
# Date: Oct. 21, 2019

# ver 0.2
# added intermediate publishers to publish the messages of different pedestrians to corresponding topics



import rospy
# from std_msgs.msg import String
from decawave.msg import position
from std_msgs.msg import Float64MultiArray
from pii.msg import trajectory
import numpy as np
import bisect
import time

gem_global_pos = []
# ped_rel_pos = []
ped_global_pos = []

xc = 7.1641413274
yc = -7.87052299175
Rc = 5.23862482537

intent = np.array([5.95, -10.75])

# here we must assume a fixed number of publishers.
pub = []
pub.append(rospy.Publisher('ped_global_pos_0', Float64MultiArray, queue_size=10))
pub.append(rospy.Publisher('ped_global_pos_1', Float64MultiArray, queue_size=10))
pub.append(rospy.Publisher('ped_global_pos_2', Float64MultiArray, queue_size=10))
pub.append(rospy.Publisher('ped_global_pos_3', Float64MultiArray, queue_size=10))

def gem_callback(data):

    # rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)
    if data.data != (999., 999.):
	tmp_data = list(data.data)
	tmp_data.insert(0, data.time) # [t, x, y]
	tmp_data = np.array(tmp_data)

	drct = np.array(tmp_data[1:]) - np.array([xc, yc])
	drct = drct / np.linalg.norm(drct)

	y_drct = np.array([drct[1], -drct[0]])
	x_drct = -drct

	tmp_data = np.append(tmp_data, x_drct)
	tmp_data = np.append(tmp_data, y_drct)

	gem_global_pos.append(tmp_data) # [t, x, y, x_drct, y_drct]
	# print('gem')
	# print([data.time, data.data])  # [time_stamp, (x, y)]

def ped_coordinate_transform_callback(data):

    ped_data = list(data.data) # [curr_time] + [length] + ids + labels + positions_x + positions_y
    ped_timestamp = ped_data[0]
    ped_num = int(ped_data[1])
    ped_ids = np.array(ped_data[2:2+ped_num]).astype(int)
    # ped_labels = ped_data[2+ped_num:2+2*ped_num] # currently useless
    ped_relative_pos_xs = ped_data[2+2*ped_num:2+4*ped_num]
    ped_relative_pos_ys = ped_data[2+4*ped_num:2+6*ped_num]

    if ped_ids[0] != -888:
    	# gem_global_pos.append(tmp_data) # [t, x, y, x_drct, y_drct]
    	# gem_global_pos_t is np.array
        gem_global_pos_t = gem_global_pos[-1][1:3] # [x, y]
        gem_x_dir = gem_global_pos[-1][3:5] # x_drct
        gem_y_dir = gem_global_pos[-1][5:] # y_drct
    	for i in range(ped_num):
    		ped_id = ped_ids[i] # already set as int in astype step
    		ped_relative_pos_x = ped_relative_pos_xs[i]
    		ped_relative_pos_y = ped_relative_pos_ys[i]
    		ped_global_pos_t = gem_global_pos_t + ped_relative_pos_x * gem_x_dir + ped_relative_pos_y * gem_y_dir # np.array (2, )
			msg = Float64MultiArray()
			msg.data = [ped_id, int(ped_timestamp)] + list(ped_global_pos_t)
			pub[ped_id].publish(msg)
		print('ped_coordinate_transform_callback')

if __name__ == '__main__':

    rospy.init_node('coordinate_transform', anonymous=True)
    rospy.Subscriber('gem_location', position, gem_callback)
    # rospy.Subscriber('object_pos', Float64MultiArray, ped_callback)
    rospy.Subscriber('tracking_results', Float64MultiArray, ped_coordinate_transform_callback)
    # [curr_time] + [length] + ids + labels + positions_x + positions_y
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
