#!/usr/bin/env python

#from decawave.msg import position, PositionWithSpeed
#from pii.msg import trajectory
import numpy as np
import math
from math import cos, atan, pi
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.animation as animation
from matplotlib import style
import rospy
from std_msgs.msg import String, Bool, Float32, Float64, Char, UInt64, Float64MultiArray
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from scipy.integrate import odeint
import scipy as scipy
import scipy.special as sc
import warnings
import time 
from statistics import mean 
import cvxpy
import cvxopt
import random
#import glpk
import subprocess
# from k_gamma_calc import k_gamma_calc
import copy
import pickle
from GetOffset import OffsetCalculator
from NewPIDController import PID_controller
from decawave.msg import trajectory


reachability_pub = rospy.Publisher('/gem_reachability', Char, queue_size=1)
control_pub = rospy.Publisher('/gem_decision', Char, queue_size=1)

# Ratio between physical wheel angle to steering wheel angle
global wheel_to_steering_ratio
wheel_to_steering_ratio = 0.0506145
wheel_radius = 0.32
wheel_inertia = 1.0
max_steering_angle = 0.4 #(max_wheel*wheel_to_steering_ratio)
global car_state_position
car_state_position = 3

class reachability_analysis:
    def __init__(self, controller=PID_controller(st_p=5.0, st_i=0.0, \
            st_d=2.7, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=1.0)):
        self.car_update_time = 0
        #lane status
        self.radius = 5.24
        #car dimension
        self.car_length = 5

        #car status
        self.car_x = 0
        self.car_prev_x = []
        self.car_y = 0
        self.car_prev_y = []
        self.car_theta = 0
        self.car_prev_thetas = []
        self.car_delta = 0
        self.car_prev_deltas = []
        self.car_v = 0
        self.car_prev_v = [] 
        self.car_offset = 0
        self.car_prev_offset = []

        #pedestrian status
        #each pedestrian has unique id
        self.pedestrian_trajectory_dict = {}
        self.pedestrian_tube_dict = {}
        self.pedestrian_tube = None

        self.n = 100

        #pid_controller = PID_controller(p=1.6, i=0, d=0.7, sp_p=1.35, sp_d=0.25, sp_i=0.08, desired_speed=0.3) #params for straight line
        self.pid_controller = controller #params for curv
        self.offsetCalculator = OffsetCalculator()

        #modes = ['Accel','Cruise','Brake','EMB']
        self.command_dict = {'Cruise': 100, 'Brake': 115, 'EMB':115, 'Accel':100}

    def publish_command(self, command):
        print('Command:',command)
        msg = Char()
        msg.data = self.command_dict.get(command, 100)
        control_pub.publish(msg)

    def update_lane_offset(self, lane_input):
        self.car_prev_offset.append(lane_input.data)
        if len(self.car_prev_offset) > 3:
            self.car_prev_offset.pop(0)
            self.car_offset = mean(self.car_prev_offset)

    def update_car_status(self, car_inputs):
        self.car_update_time = int((car_inputs.data[0])/1000)

        self.car_prev_x.append(car_inputs.data[1])
        if len(self.car_prev_x) > 5:
            self.car_prev_x.pop(0)
            self.car_x = mean(self.car_prev_x)

        self.car_prev_y.append(car_inputs.data[2])
        if len(self.car_prev_y) > 5:
            self.car_prev_y.pop(0)
            self.car_y = mean(self.car_prev_y)

        potential_theta= math.atan2(car_inputs.data[-2], car_inputs.data[-1])  #from -pi/2 to pi/2
        self.car_prev_thetas.append(potential_theta)

    def update_car_velocity(self, speed_input):
        self.car_prev_v.append(speed_input.data)
        if len(self.car_prev_v) > 5:
            self.car_prev_v.pop(0)
            self.car_v = mean(self.car_prev_v)  # float

    def update_car_steer(self, steer_input):
        self.car_prev_deltas.append(steer_input.angular_position)
        if len(self.car_prev_deltas) > 5:
            self.car_prev_deltas.pop(0)
            self.car_delta = mean(self.car_prev_deltas)

    def update_pedestrians(self, pedestrian_inputs):
        #pedestrian_inputs is a list of pedestrian tuples
        #suppose pedestrian tuple (id, time, np.array([x1,x2,x3...]), np.array([y1,y2,y3...]))
        for pedestrian_tuple in pedestrian_inputs:
            ped_id = pedestrian_tuple[0]
            start_time = pedestrian_tuple[1]
            pedestrian_traj = []
            for i in range(pedestrian_tuple[2].shape[0]):
                pedestrian_traj.append([int(start_time/1000 + i*0.1), pedestrian_tuple[2][i], pedestrian_tuple[3][i]])
            pedestrian_traj = np.array(pedestrian_traj)
            self.pedestrian_trajectory_dict[ped_id] = pedestrian_traj
            self.pedestrian_tube_dict[ped_id] = self.bloatToTube([2,2],[0,0],[0.1,0.1],pedestrian_traj,3)

    def update_pedestrian(self, pedestrian_inputs):
        pedestrian_id = pedestrian_inputs.x[0]
        self.pedestrian_start_ts = pedestrian_inputs.time
        #pedestrian_trajectory_x = pedestrian_inputs.data[2]
        #pedestrian_trajectory_y = pedestrian_inputs.data[3]

        pedestrian_traj = []
        for i in range(1, len(pedestrian_inputs.x)):
            pedestrian_traj.append([int(self.pedestrian_start_ts/1000 + i*0.1),\
                 pedestrian_inputs.x[i], pedestrian_inputs.y[i]])
        pedestrian_traj = np.array(pedestrian_traj)

        self.pedestrian_trajectory_dict[pedestrian_id] = pedestrian_traj
        self.pedestrian_tube_dict[pedestrian_id] = self.bloatToTube([0,0],[0,0],[0,0],pedestrian_traj,3)

    def get_pedestrian_tubes(self):
        return self.pedestrian_tube_dict

    #for gazebo only
    def gazebo_car_satus(self, data):
        car_x = data.pose[car_state_position].position.x  # vehicle x position
        car_y = data.pose[car_state_position].position.y  # vehicle y position
        # calculate heading angle
        dx = car_x - self.car_x
        dy = car_y - self.car_y
        self.car_x, self.car_y = car_x, car_y
        #print (car_x, car_y)
        #if (dx >= 0 and dy >= 0) or (dx > 0 and dy < 0):
        self.car_theta = math.atan2(dx,dy)
        # elif dx < 0 and dx > 0:
        #     self.car_theta = atan(dx / dy) + pi
        # else:
        #     self.car_theta = atan(dx / dy) - pi

    def gazebo_car_speed(self, data):
        self.car_v = (data.velocity[4] + data.velocity[5])*0.5

    def gazebo_car_steering(self, data):
        self.car_delta = data.data
    #####
    #bicycle model
    def Car_dynamic_b(self, y, t, acc, w):
        # make everything double
        acc = float(acc)
        w = float(w)
        # end of making float

        sx, sy, delta, v, psi = y
        if delta > max_steering_angle:
            delta = max_steering_angle
        elif delta < max_steering_angle:
            delta = max_steering_angle

        #print v
        # psi is the angle between y axis and v
        # y
        # |psi/
        # |  /
        # | /
        # |/__________ x
        sx_dot = v * np.sin(psi)
        sy_dot = v * np.cos(psi)
        delta_dot = w
        psi_dot = v/self.car_length * np.tan(delta)
        dydt = [sx_dot, sy_dot, delta_dot, acc, psi_dot]
        return dydt

    #dubin's model
    def Car_dynamic_d(self, y, t, acc, w):
        # make everything double
        #print(acc, w)
        acc = float(acc)
        w = float(w)
        # end of making float

        sx, sy, v, psi = y

        #print v
        # psi is the angle between y axis and v
        # y
        # |psi/
        # |  /
        # | /
        # |/__________ x
        sx_dot = v * np.sin(psi)
        sy_dot = v * np.cos(psi)
        psi_dot = v/self.car_length * np.tan(w)
        #print("v:", v)
        dydt = [sx_dot, sy_dot, acc, psi_dot]
        return dydt

    def Car_simulate(self, time_bound):
        #print (self.car_x, self.car_y, self.car_delta, self.car_theta, self.car_v)
        #radius = self.radius
        time_step = 0.025
        num_of_seg = 50
        time_bound = float(time_bound)
        mode_change_interval = 50 #the time interval to change mode

        number_points = int(np.ceil(time_bound/time_step))
        t = [self.car_update_time+i*time_step for i in range(0,number_points)]
        if t[-1] != self.car_update_time+time_step:
            t.append(self.car_update_time+time_bound)
        newt = []
        for step in t:
            newt.append(float(format(self.car_update_time+step, '.2f')))
        t = newt
        if t[-1] == t[-2]:
            t.pop()
        
        points_in_seg = int(np.ceil(number_points/num_of_seg))
        dis_time_step = points_in_seg * time_step
        t_small = [i*time_step for i in range(0,points_in_seg)]
        if t_small[-1] != time_step:
            t_small.append(dis_time_step)
        newt_small = []
        for step in t_small:
            newt_small.append(float(format(step, '.2f')))
        t_small = newt_small
        if t_small[-1] == t_small[-2]:
            t_small.pop()

        #initialize the state
        # k = [1,1,1,1]
        # gamma = [0,0,0,0]
        # init_delta_array = [0.5,0.5,0.1,0.1]
        k = [1,1,1,1,1]
        gamma = [0,0,0,0,0]
        init_delta_array = [1,1,0.1,0.1,0.1]
        prev_delta = 0

        dimensions = 5

        sx_initial = self.car_x
        sy_initial = self.car_y
        delta_initial = self.car_delta
        v_initial = self.car_v
        psi_initial = self.car_theta
        acc = 0.0
        modes = ['Cruise']#,'Accel','Brake','EMB']
        w = 0
        offset = self.offsetCalculator.GetOffset([sx_initial, sy_initial, psi_initial])

        initial_ode = [sx_initial, sy_initial, delta_initial, v_initial, psi_initial]
        #find center of the circle
        #center_x, center_y = sx_initial+(offset+radius)*math.cos(psi_initial), sy_initial+(offset+radius)*math.sin(psi_initial)


        def propagate_states(cur_states, sols):
            new_states = []
            for state in cur_states:
                for mode in modes:
                    new_mode_arr = copy.deepcopy(state[0])
                    new_mode_arr.append(mode)
                    new_states.append((new_mode_arr,state[1],state[2]))
            new_sols = []
            for cur_sol in sols:
                for i in range(len(modes)):
                    new_sols.append(cur_sol) 
            return new_states, new_sols


        empty_list = []
        states = [(empty_list, initial_ode, offset)]
        sols = [np.array([])] 
        trace = []
        tube100 = None
        tube75 = None
        tube50 = None
        prev_delta = 0
        #print("Simulation Start")

        #epsilon: error between previous prediction and current position
        # dim_state = 5
        # eps = [0] * dim_state
        # if self.prev_states:
        #     for i in range(dim_state):
        #         eps.append(self.prev_states[4][i]-initial_ode[i])
        # else:
        #     for i in range(dim_state):
        #         eps[i] = -1 * initial_ode[i]
        #     self.prev_states = [[0]*dim_state]*121

        # #subtract epsilon from all previous states
        # p = 0.5
        # for i in range(len(self.prev_states)):
        #     subtracted_state = self.prev_states[i]
        #     for j in range(dim_state):
        #         subtracted_state[j] -= eps[j]
        #     self.prev_states[i] = subtracted_state

        #number_points
        #num_of_seg
        for i in range(1+int(number_points/mode_change_interval)):
            states, sols = propagate_states(states, sols)
            #print(states)
            for j in range(len(states)):
                cur_state = states[j]
                # print("states", states)
                # print(j)
                cur_sol = sols[j]
                cur_mode = cur_state[0][-1]
                x, y = cur_state[1][0], cur_state[1][1]
                for tk in range(mode_change_interval):
                    delta = self.pid_controller.steer_control(x, y, cur_state[1][4], dis_time_step)
                    cur_state[1][2] = delta
                    w = (delta - prev_delta) / dis_time_step
                    #w = self.pid_controller.steer_control(x, y, dis_time_step)
                    # acc = self.pid_controller.speed_control(float(cur_state[1][2]),dis_time_step,cur_mode)
                    acc = self.pid_controller.speed_control(float(cur_state[1][3]),dis_time_step,cur_mode)
                    sol_temp = odeint(self.Car_dynamic_b,cur_state[1],t_small,args=(acc, w),hmax = time_step)
                    initial_ode = sol_temp[-1,:]
                    sol_temp = sol_temp[:-1,:]
                    if tk < mode_change_interval - 1:
                        sol_temp = sol_temp[:-1,:]
                    if cur_sol.shape[0] == 0:
                        cur_sol = sol_temp
                    else:
                        cur_sol = np.vstack((cur_sol, sol_temp))
                    # x, y, psi = initial_ode[0], initial_ode[1], initial_ode[2] # Hussein: uncomment for the dubins vehicle
                    x, y, psi = initial_ode[0], initial_ode[1], initial_ode[4]
                    #car_to_center = math.sqrt((x-center_x)**2 + (y-center_y)**2)
                    offset = self.offsetCalculator.GetOffset([x,y,psi])
                    #print ("offset:", offset, x, y, psi)
                    if offset > 5:
                        offset = 5
                    elif offset < -5:
                        offset = -5
                    #offset = car_to_center - radius
                    prev_delta = delta #initial_ode[2]
                    #update the state
                    cur_state = (cur_state[0],initial_ode,offset)

                #check safety if trajectory is finished
                #num_of_seg/mode_change_interval,  number_points
                if i == int(number_points/mode_change_interval):
                    # length of t should be equal to length of sol
                    cur_t = t
                    while(len(cur_t) < cur_sol.shape[0]):
                        cur_sol = np.delete(cur_sol, -1, axis=0)
                    while(cur_sol.shape[0] < len(cur_t)):
                        cur_t = np.delete(cur_t, -1, axis=0)

                    # print (sol)
                    #print(sol.shape)
                    trace = []
                    for tj in range(len(cur_t)):
                        tmp = []
                        tmp.append(int(cur_t[tj]))
                        tmp.append(cur_sol[tj, 0])
                        tmp.append(cur_sol[tj, 1])
                        tmp.append(cur_sol[tj, 2])
                        tmp.append(cur_sol[tj, 3])
                        tmp.append(cur_sol[tj, 4])
                        trace.append(tmp)

                    init_delta_array75 = [x*0.75 for x in init_delta_array]
                    init_delta_array50 = [x*0.5 for x in init_delta_array]

                    #k, gamma = self.Global_Discrepancy(init_delta_array75, traces)
                    tube100 = self.bloatToTube(k, gamma, init_delta_array, trace, dimensions)
                    tube75 = self.bloatToTube(k, gamma, init_delta_array75, trace, dimensions)
                    tube50 = self.bloatToTube(k, gamma, init_delta_array50, trace, dimensions)

                    safety100 = self.checkSafety(tube100)
                    safety75 = self.checkSafety(tube75)
                    safety50 = self.checkSafety(tube50)
                    if safety100 and safety75 and safety50:
                        #self.publish_command(cur_state[0][0])
                        #print('All Commands', cur_state[0])
                        #output[i] = p*old_trace[i+1] + (1-p)*new_trace[i]
                        # combined_trace = []
                        # len_trace = len(trace)
                        # len_prev_trace = len(self.prev_states)
                        # print(len_trace, len_prev_trace)
                        # for i in range(min(len_trace-1, len_prev_trace-1)):
                        #     new_point = [0] * dim_state
                        #     for j in range(dim_state):
                        #         new_point[j] = p*self.prev_states[i+1][j] + (1-p)*trace[i][j]
                        #     combined_trace.append(new_point)
                        # # if len_trace > len_prev_trace:
                        # #     combined_trace.append(trace[-1])
                        # #combined_trace.append(trace[-1])
                        # self.prev_states = combined_trace
                        # return combined_trace, tube100, tube75, tube50, True
                        return trace, tube100, tube75, tube50, True
                    
                else:  #update and store states for future use
                    states[j] = cur_state
                    sols[j] = cur_sol

        #self.publish_command("EMB")
        print ("Brake!")

        #output[i] = p*old_trace[i+1] + (1-p)*new_trace[i]
        # combined_trace = []
        # for i in range(len(trace)-1):
        #     new_point = [0] * dim_state
        #     for j in range(dim_state):
        #         new_point[j] = p * self.prev_states[i+1][j] + (1-p) * trace[i][j]
        #     combined_trace.append(new_point)
        # #combined_trace.append(trace[-1])
        # self.prev_states = combined_trace

        # return combined_trace, tube100, tube75, tube50, False
        return trace, tube100, tube75, tube50, False


    def bloatToTube(self, k, gamma, init_delta_array, trace, dimensions):
        center_trace = trace
        reach_tube = []
        trace_len = len(trace)
        for i in range(trace_len-1):
            #pdb.set_trace()
            time_interval = center_trace[i+1][0] - center_trace[0][0]
            lower_rec = [center_trace[i][0]]
            upper_rec = [center_trace[i+1][0]]

            for dim in range(1,dimensions):
                delta = k[dim-1] * math.exp(gamma[dim-1]*time_interval) * init_delta_array[dim-1]
                upper_rec.append(max(center_trace[i+1][dim],center_trace[i][dim])+delta)
                lower_rec.append(min(center_trace[i+1][dim],center_trace[i][dim])-delta)
            reach_tube.append(lower_rec)
            reach_tube.append(upper_rec)
        return reach_tube

    def read_data(self, traces):
        # Locals
        trace = traces[0]
        error_thred_time = 1e-6

        # Calculate variables
        self.dimensions = len(trace[0])
        self.dimensions_nt = self.dimensions - 1
        #trace_len = len(trace)
        #start_time = trace[0][0]
        self.end_time = trace[-1][0]

        # Align all the traces
        for i in range (len(traces)):
            initial_time = traces[i][0][0]
            for j in range (len(traces[i])):
                traces[i][j][0] = traces[i][j][0] - initial_time

        #reasign the start time and end time
        self.start_time = 0
        for i in range (len(traces)):
            self.end_time = min(self.end_time,traces[i][-1][0])

        #trim all the points after the end time
        traces_trim = []
        for i in range (len(traces)):
            trace_trim = []
            for j in range (len(traces[i])):
                if traces[i][j][0] <= self.end_time + error_thred_time:
                    trace_trim.append(traces[i][j])
            traces_trim.append(trace_trim)
            #print(len(trace_trim))

        #reasign trace_len
        self.trace_len = len(trace_trim)

        return traces_trim

    def doRectsInter(self, rect1, rect2):
        for i in range(len(rect1[0][:])):
            if rect1[0][i] > rect2[1][i] or rect1[1][i] < rect2[0][i]:
                return False
        return True

    # assuming that the unsafesets time is aligned with the time of the tube
    def checkSafety(self, tube):
        if tube:
            for id, unsafeset in self.pedestrian_tube_dict.items():
                for i in range(0,len(tube)-1,2):
                    for j in range(0,len(unsafeset)-1,2):
                        #print("Unsafe set:", [unsafesets[j], unsafesets[j+1]])
                        #print("tube: ", [tube[i][:3], tube[i+1][:3]])
                        if self.doRectsInter([unsafeset[j][1:], unsafeset[j+1][1:]], [tube[i][1:3], tube[i+1][1:3]]):
                            return False
        return True


if __name__ == '__main__':
    #pid_controller = PID_controller(st_p=5.7, st_i=0.0, st_d=3.5, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=10.0)
    pid_controller = PID_controller(st_p=5.0, st_i=0.0, st_d=2.7, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=0.1)
    reachability_analyzer = reachability_analysis(controller=pid_controller)
   
    rospy.init_node('Reachability_Analysis')
    rospy.Subscriber("/gazebo/model_states", ModelStates, reachability_analyzer.gazebo_car_satus, queue_size=1)
    rospy.Subscriber("/polaris/joint_states", JointState, reachability_analyzer.gazebo_car_speed, queue_size=1)
    rospy.Subscriber("/polaris/front_left_steering_position_controller/command", Float64, reachability_analyzer.gazebo_car_steering, queue_size=1)
    rospy.Subscriber("/predicted_trajectory",trajectory, reachability_analyzer.update_pedestrian, queue_size=1)

    #rospy.Subscriber("/lane_deviation", Float32, reachability_analyzer.update_lane_offset)
    #rospy.Subscriber("/pacmod/as_rx/steer_cmd", PositionWithSpeed, reachability_analyzer.update_car_steer)
    #rospy.Subscriber("/gem_location", position, decision_maker.update_ego_car)
    #rospy.Subscriber("/gem_global_status", Float64MultiArray, reachability_analyzer.update_car_status)
    #rospy.Subscriber("/predicted_trajectory", trajectory, reachability_analyzer.update_pedestrian)
    #rospy.Subscriber("/pacmod/as_tx/vehicle_speed", Float64, reachability_analyzer.update_car_velocity)
    # rospy.Subscriber("/object_pos", Float64MultiArray, decision_maker.update_single_ped)
    
    print("Reachability node started!")
    # [sx, sy, delta, v, psi]1
    time_bound = 3

    # plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    color_reach = 'c'
    color_reach75 = 'g'
    color_reach50 = 'b'
    color_ped = 'r'
    color_lane = 'grey'
    plt.rcParams.update({'font.size': 12})
    # plt.xlim((0, 30))
    # plt.ylim((0, 15))
    plt.xlim((-10, 10))
    plt.ylim((-10, 10))
    plt.ylabel('y (m)', fontsize=15)
    plt.xlabel('x (m)',fontsize=15)
    plt.grid('on')


    with open('obstacle.data', 'rb') as filehandle:
        # read the data as binary data stream
        obstacle = pickle.load(filehandle)
    
    ox, oy = [], []
    # for (x,y) in obstacle:
    #     ox.append(x)
    #     oy.append(y)

    data = np.loadtxt('sparse_lane_data.dat')#('eceb_4.dat')##('lane_data.dat')
    smoothed_x = data[:, 0]-7
    smoothed_y = data[:, 1]+8
    lane_trace = np.hstack((np.zeros((smoothed_x.shape[0],1)),smoothed_x.reshape((smoothed_x.shape[0],1)),smoothed_y.reshape((smoothed_y.shape[0],1))))
    #print("lane trace shape: ",smoothed_y.shape)
    lane_tube = reachability_analyzer.bloatToTube([1,1],[1,1],[1,1],lane_trace,3)
    
    car_position = np.loadtxt('gazebo_car_position.dat')
    car_x = car_position[:, 0]
    car_y = car_position[:, 1]
    car_real_trace = np.hstack((np.zeros((car_x.shape[0],1)),car_x.reshape((car_x.shape[0],1)),car_y.reshape((car_y.shape[0],1))))    


    #print("lane data loaded:", lx.size, ly.size)
    #smoothed_x = np.array([])
    #smoothed_y = np.array([])
    #lane_fit_coeff = [0.19405614, -2.73941924, -5.44065305]
    #smoothed_x = np.linspace(0.12, 13, 100)
    #smoothed_y = lane_fit_coeff[0]*smoothed_x**2 + lane_fit_coeff[1]*smoothed_x + lane_fit_coeff[2]
    #for i in range(5, lx.size):
    #    avg_x = np.mean(lx[i-5:i+1])
    #    avg_y = np.mean(ly[i-5:i+1])
    #    smoothed_x = np.append(smoothed_x, avg_x)
    #    smoothed_y = np.append(smoothed_y, avg_y)

    #######Fake pedestrian data, comment out when using actual data###############
    #(id, time, np.array([x1,x2,x3...]), np.array([y1,y2,y3...]))
    # data = np.loadtxt('ped_data.dat')
    # lx1 = data[:, 0]
    # ly1 = data[:, 1]-0.8
    # tuple1 = (1, 1570227096, np.array(lx1), np.array(ly1))

    # data = np.loadtxt('ped_fake.dat')
    # lx2 = data[:, 0]
    # ly2 = data[:, 1]-0.8
    # tuple2 = (2, 1570227096, np.array(lx2), np.array(ly2))
    
    # reachability_analyzer.update_pedestrians([tuple1, tuple2])
    ################################


    def runtime(i):
        if not rospy.core.is_shutdown():
            start = time.time()
            # unsafesets = [[[tmin1, xmin1, ymin1],[tmax1, xmax1, ymax1]], [[tmin2, xmin2, ymin2],[tmax2, xmax2, ymax2]]]
            # #print(np.array(trace)[:,1:3])

            trace, tube100, tube75, tube50, safe = reachability_analyzer.Car_simulate(time_bound)
            #print (trace)
            ax.clear()

            currentAxis = plt.gca()
            # plt.xlim((0, 30))
            # plt.ylim((0, 15))
            plt.xlim((-10, 10))
            plt.ylim((-10, 10))
            if lane_tube:
                for i in range(0, len(lane_tube)-1, 2):
                    w = lane_tube[i+1][1] - lane_tube[i][1]
                    h = lane_tube[i+1][2] - lane_tube[i][2]
                    rect = Rectangle((lane_tube[i][1],lane_tube[i][2]),w,h,linewidth=3,edgecolor=color_lane,facecolor=color_lane, alpha=0.3)
                    if i == 0:
                        lane_rect_label = rect
                    #rect.set_label('pedestrian trajectory')
                    # add the patch to axes
                    currentAxis.add_patch(rect)
            if trace:
                x = np.array(trace)[:,1]
                y = np.array(trace)[:,2]
                # y = [x-5.5 for x in y]
            else:
                x, y = [], []

            ax.plot(x, y, 'k.', car_x, car_y, '^b', linewidth=4)
            ax.plot(ox, oy, '.k', smoothed_x, smoothed_y, 'y--')
            #print("Safety:", safe)
            if tube100 and tube75 and tube50:
                for i in range(0, len(tube100)-1, 2):
                    w100 = tube100[i+1][1] - tube100[i][1]
                    h100 = tube100[i+1][2] - tube100[i][2]
                    w75 = tube75[i+1][1] - tube75[i][1]
                    h75 = tube75[i+1][2] - tube75[i][2]
                    w50 = tube50[i+1][1] - tube50[i][1]
                    h50 = tube50[i+1][2] - tube50[i][2]
                    rect100 = Rectangle((tube100[i][1],tube100[i][2]),w100,h100,linewidth=1,edgecolor=color_reach,facecolor='none', alpha=0.5)
                    rect75 = Rectangle((tube75[i][1],tube75[i][2]),w75,h75,linewidth=1,edgecolor=color_reach75,facecolor='none', alpha=0.75)
                    rect50 = Rectangle((tube50[i][1],tube50[i][2]),w50,h50,linewidth=1,edgecolor=color_reach50,facecolor='none')

                    # add the patch to axes
                    currentAxis.add_patch(rect100)
                    currentAxis.add_patch(rect75)
                    currentAxis.add_patch(rect50)
                    if i == 0:
                        car_rect_label100 = rect100
                        car_rect_label75 = rect75
                        car_rect_label50 = rect50

            cur_time = time.time()-start
            #print("This time:", cur_time)
            #trace_plt.set_label('a')
            #lane_plt.set_label('b')

            # pedestrian_tube = None
            # if reachability_analyzer.pedestrian_trajectory.shape[0] > 0:
            pedestrian_tubes = reachability_analyzer.get_pedestrian_tubes()
            for id, pedestrian_tube in pedestrian_tubes.items():
                for i in range(0, len(pedestrian_tube)-1, 2):
                    w = pedestrian_tube[i+1][1] - pedestrian_tube[i][1]
                    h = pedestrian_tube[i+1][2] - pedestrian_tube[i][2]
                    rect = Rectangle((pedestrian_tube[i][1],pedestrian_tube[i][2]),w,h,linewidth=3,edgecolor=color_ped,facecolor='none')
                    if i == 0:
                        ped_rect_label = rect
                    #rect.set_label('pedestrian trajectory')
                    # add the patch to axes
                    currentAxis.add_patch(rect)

            # if pedestrian_tubes and not tube100:
            #     plt.legend((trace_plt, lane_plt, ped_rect_label), ('Predicted Car trajectory','Lane center','predicted pedestrian trajectory'))
            # elif tube100 and tube75 and tube50 and not pedestrian_tubes:
            #     plt.legend((trace_plt, lane_plt, car_rect_label100, car_rect_label75, car_rect_label50),\
            #          ('Predicted Car trajectory','Lane center','High confidence car reachtube', 'Medium confidence car reachtube','Low confidence car reachtube'))
            # elif tube100 and tube75 and tube50 and pedestrian_tubes:
            #     plt.legend((trace_plt, lane_plt, ped_rect_label, car_rect_label100, car_rect_label75, car_rect_label50),\
            #           ('Predicted Car trajectory','Lane center', 'predicted pedestrian trajectory', 'High confidence car reachtube', \
            #               'Medium confidence car reachtube','Low confidence car reachtube'))
            # else:
            #     plt.legend((trace_plt, lane_plt), ('Predicted Car trajectory','Lane center'))

            plt.draw()
            #print(tube)
        # rospy.rostime.wallsleep(1)

    # Set up formatting for the movie files
    # Writer = animation.writers['ffmpeg']
    # writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
    ani = animation.FuncAnimation(fig, runtime, frames=180)
    # ani.save('im.mp4', writer=writer)
    plt.show()