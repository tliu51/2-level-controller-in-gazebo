import numpy as np
import math
from math import cos, atan, pi
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.animation as animation
from matplotlib import style
from scipy.integrate import odeint
import scipy as scipy
import scipy.special as sc
import warnings
import time 
from statistics import mean 
import cvxpy
import cvxopt
import random
#import glpk
import subprocess
import copy
from NewGetOffset import GetOffset
from NewPIDController import PID_controller
import pickle
from simple_decision import simple_decision


# Ratio between physical wheel angle to steering wheel angle
global wheel_to_steering_ratio
wheel_to_steering_ratio = 0.0506145
wheel_radius = 0.32
wheel_inertia = 1.0
max_steering_angle = 0.4 #(max_wheel*wheel_to_steering_ratio)


class reachability_analysis:
    def __init__(self, controller=PID_controller()):
        self.car_update_time = 0
        #lane status
        self.radius = 5.24
        #car dimension
        self.car_length = 3

        #car status
        self.car_x = 0
        self.car_prev_x = []
        self.car_y = 0
        self.car_prev_y = []
        self.car_theta = 0
        self.car_prev_thetas = []
        self.car_delta = 0
        self.car_prev_deltas = []
        self.car_v = 0
        self.car_prev_v = [] 
        self.car_offset = 0
        self.car_prev_offset = []

        #pedestrian status
        #each pedestrian has unique id
        self.pedestrian_trajectory_dict = {}
        self.pedestrian_tube_dict = {}
        self.pedestrian_tube = None

        self.n = 100

        #pid_controller = PID_controller(p=1.6, i=0, d=0.7, sp_p=1.35, sp_d=0.25, sp_i=0.08, desired_speed=0.3) #params for straight line
        self.pid_controller = controller #params for curv

        #modes = ['Accel','Cruise','Brake','EMB']
        self.command_dict = {'Cruise': 100, 'Brake': 115, 'EMB':115, 'Accel':100}

        #store states
        self.prev_states = []

    def update_car_status(self, car_inputs):
        self.car_update_time = car_inputs[-2][0]
        # self.car_x = car_inputs[0][0]
        # self.car_y = car_inputs[0][1]
        carv_x, carv_y = car_inputs[-4][0], car_inputs[-4][1]
        self.car_v = np.sqrt(carv_x**2 + carv_y**2)

        self.car_prev_x.append(car_inputs[0][0])
        if len(self.car_prev_x) > 3:
            self.car_prev_x.pop(0)
        self.car_x = mean(self.car_prev_x)

        self.car_prev_y.append(car_inputs[0][1])
        if len(self.car_prev_y) > 3:
            self.car_prev_y.pop(0)
        self.car_y = mean(self.car_prev_y)

        self.car_prev_thetas.append(math.atan2(carv_x,carv_y))
        if len(self.car_prev_thetas) > 3:
            self.car_prev_thetas.pop(0)
        self.car_theta = mean(self.car_prev_thetas)

        if car_inputs[-1][0] == None:
            self.car_prev_deltas.append(0)
        else:
            self.car_prev_deltas.append(car_inputs[-1][0])
        if len(self.car_prev_deltas) > 3:
            self.car_prev_deltas.pop(0)
        self.car_delta = mean(self.car_prev_deltas)        

        # self.car_theta = math.atan2(carv_x,carv_y)
        # self.car_delta = car_inputs[5][0]

    def update_car_steer(self, steer_input):
        self.car_prev_deltas.append(steer_input.angular_position)
        if len(self.car_prev_deltas) > 5:
            self.car_prev_deltas.pop(0)
            self.car_delta = mean(self.car_prev_deltas)

    def update_pedestrians(self, pedestrian_inputs):
        #pedestrian_inputs is a list of pedestrian tuples
        #suppose pedestrian tuple (id, time, np.array([x1,x2,x3...]), np.array([y1,y2,y3...]))
        for pedestrian_tuple in pedestrian_inputs:
            ped_id = pedestrian_tuple[0]
            start_time = pedestrian_tuple[1]
            pedestrian_traj = []
            for i in range(pedestrian_tuple[2].shape[0]):
                pedestrian_traj.append([int(start_time/1000 + i*0.5), pedestrian_tuple[2][i], pedestrian_tuple[3][i]])
            pedestrian_traj = np.array(pedestrian_traj)
            self.pedestrian_trajectory_dict[ped_id] = pedestrian_traj
            self.pedestrian_tube_dict[ped_id] = self.bloatToTube([2,2],[0,0],[0.1,0.1],pedestrian_traj,3)

    def update_pedestrian(self, pedestrian_inputs):
        self.pedestrian_start_ts = 0

        pedestrian_traj = []
        for i in range(len(pedestrian_inputs)):
            pedestrian_traj.append([self.pedestrian_start_ts+i*0.5, pedestrian_inputs[i][0], pedestrian_inputs[i][1]])
        pedestrian_traj = np.array(pedestrian_traj)
        #print (pedestrian_traj)

        self.pedestrian_trajectory_dict[0] = pedestrian_traj
        self.pedestrian_tube_dict[0] = self.bloatToTube([0,0],[0,0],[0,0],pedestrian_traj,3)

    def get_pedestrian_tubes(self):
        return self.pedestrian_tube_dict

    #bicycle model
    def Car_dynamic_b(self, y, t, acc, w):
        # make everything double
        acc = float(acc)
        w = float(w)
        # end of making float

        sx, sy, delta, v, psi = y
        if delta > max_steering_angle:
            delta = max_steering_angle
        elif delta < max_steering_angle:
            delta = max_steering_angle

        #print v
        # psi is the angle between y axis and v
        # y
        # |psi/
        # |  /
        # | /
        # |/__________ x
        sx_dot = v * np.sin(psi)
        sy_dot = v * np.cos(psi)
        delta_dot = w
        psi_dot = v/self.car_length * np.tan(delta)
        dydt = [sx_dot, sy_dot, delta_dot, acc, psi_dot]
        return dydt

    #dubin's model
    def Car_dynamic_d(self, y, t, acc, w):
        # make everything double
        #print(acc, w)
        acc = float(acc)
        w = float(w)
        # end of making float

        sx, sy, v, psi = y

        #print v
        # psi is the angle between y axis and v
        # y
        # |psi/
        # |  /
        # | /
        # |/__________ x
        sx_dot = v * np.sin(psi)
        sy_dot = v * np.cos(psi)
        psi_dot = v/self.car_length * np.tan(w)
        #print("v:", v)
        dydt = [sx_dot, sy_dot, acc, psi_dot]
        return dydt

    def Car_simulate(self, time_bound):
        #print (self.car_x, self.car_y, self.car_delta, self.car_theta, self.car_v)
        radius = self.radius
        time_step = 0.025
        num_of_seg = 50
        time_bound = float(time_bound)
        mode_change_interval = 50 #the time interval to change mode

        number_points = int(np.ceil(time_bound/time_step))
        t = [self.car_update_time+i*time_step for i in range(0,number_points)]
        if t[-1] != self.car_update_time+time_step:
            t.append(self.car_update_time+time_bound)
        newt = []
        for step in t:
            newt.append(float(format(self.car_update_time+step, '.2f')))
        t = newt
        if t[-1] == t[-2]:
            t.pop()
        
        points_in_seg = int(np.ceil(number_points/num_of_seg))
        dis_time_step = points_in_seg * time_step
        t_small = [i*time_step for i in range(0,points_in_seg)]
        if t_small[-1] != time_step:
            t_small.append(dis_time_step)
        newt_small = []
        for step in t_small:
            newt_small.append(float(format(step, '.2f')))
        t_small = newt_small
        if t_small[-1] == t_small[-2]:
            t_small.pop()

        #initialize the state
        # k = [1,1,1,1]
        # gamma = [0,0,0,0]
        # init_delta_array = [0.5,0.5,0.1,0.1]
        k = [1,1,1,1,1]
        gamma = [0,0,0,0,0]
        init_delta_array = [1,1,0.1,0.1,0.1]
        prev_delta = 0

        dimensions = 5

        sx_initial = self.car_x
        sy_initial = self.car_y
        delta_initial = self.car_delta
        v_initial = self.car_v
        psi_initial = self.car_theta
        acc = 0.0
        modes = ['Cruise']#,'Accel','Brake','EMB']
        w = 0
        offset = GetOffset([sx_initial, sy_initial, psi_initial])

        initial_ode = [sx_initial, sy_initial, delta_initial, v_initial, psi_initial]
        #find center of the circle
        #center_x, center_y = sx_initial+(offset+radius)*math.cos(psi_initial), sy_initial+(offset+radius)*math.sin(psi_initial)


        def propagate_states(cur_states, sols):
            new_states = []
            for state in cur_states:
                for mode in modes:
                    new_mode_arr = copy.deepcopy(state[0])
                    new_mode_arr.append(mode)
                    new_states.append((new_mode_arr,state[1],state[2]))
            new_sols = []
            for cur_sol in sols:
                for i in range(len(modes)):
                    new_sols.append(cur_sol) 
            return new_states, new_sols


        empty_list = []
        states = [(empty_list, initial_ode, offset)]
        sols = [np.array([])] 
        trace = []
        tube100 = None
        tube75 = None
        tube50 = None
        prev_delta = 0
        #print("Simulation Start")

        #epsilon: error between previous prediction and current position
        # dim_state = 5
        # eps = [0] * dim_state
        # if self.prev_states:
        #     for i in range(dim_state):
        #         eps.append(self.prev_states[4][i]-initial_ode[i])
        # else:
        #     for i in range(dim_state):
        #         eps[i] = -1 * initial_ode[i]
        #     self.prev_states = [[0]*dim_state]*121

        # #subtract epsilon from all previous states
        # p = 0.5
        # for i in range(len(self.prev_states)):
        #     subtracted_state = self.prev_states[i]
        #     for j in range(dim_state):
        #         subtracted_state[j] -= eps[j]
        #     self.prev_states[i] = subtracted_state

        #number_points
        #num_of_seg
        for i in range(1+int(number_points/mode_change_interval)):
            states, sols = propagate_states(states, sols)
            #print(states)
            for j in range(len(states)):
                cur_state = states[j]
                # print("states", states)
                # print(j)
                cur_sol = sols[j]
                cur_mode = cur_state[0][-1]
                x, y = cur_state[1][0], cur_state[1][1]
                for tk in range(mode_change_interval):
                    delta = self.pid_controller.steer_control(x, y, cur_state[1][4], dis_time_step)
                    cur_state[1][2] = delta
                    w = (delta - prev_delta) / dis_time_step
                    #w = self.pid_controller.steer_control(x, y, dis_time_step)
                    # acc = self.pid_controller.speed_control(float(cur_state[1][2]),dis_time_step,cur_mode)
                    acc = self.pid_controller.speed_control(float(cur_state[1][3]),dis_time_step,cur_mode)
                    sol_temp = odeint(self.Car_dynamic_b,cur_state[1],t_small,args=(acc, w),hmax = time_step)
                    initial_ode = sol_temp[-1,:]
                    sol_temp = sol_temp[:-1,:]
                    if tk < mode_change_interval - 1:
                        sol_temp = sol_temp[:-1,:]
                    if cur_sol.shape[0] == 0:
                        cur_sol = sol_temp
                    else:
                        cur_sol = np.vstack((cur_sol, sol_temp))
                    # x, y, psi = initial_ode[0], initial_ode[1], initial_ode[2] # Hussein: uncomment for the dubins vehicle
                    x, y, psi = initial_ode[0], initial_ode[1], initial_ode[4]
                    #car_to_center = math.sqrt((x-center_x)**2 + (y-center_y)**2)
                    offset = GetOffset([x,y,psi])
                    #print ("offset:", offset, x, y, psi)
                    if offset > 5:
                        offset = 5
                    elif offset < -5:
                        offset = -5
                    #offset = car_to_center - radius
                    prev_delta = delta #initial_ode[2]
                    #update the state
                    cur_state = (cur_state[0],initial_ode,offset)

                #check safety if trajectory is finished
                #num_of_seg/mode_change_interval,  number_points
                if i == int(number_points/mode_change_interval):
                    # length of t should be equal to length of sol
                    cur_t = t
                    while(len(cur_t) < cur_sol.shape[0]):
                        cur_sol = np.delete(cur_sol, -1, axis=0)
                    while(cur_sol.shape[0] < len(cur_t)):
                        cur_t = np.delete(cur_t, -1, axis=0)

                    # print (sol)
                    #print(sol.shape)
                    trace = []
                    for tj in range(len(cur_t)):
                        tmp = []
                        tmp.append(int(cur_t[tj]))
                        tmp.append(cur_sol[tj, 0])
                        tmp.append(cur_sol[tj, 1])
                        tmp.append(cur_sol[tj, 2])
                        tmp.append(cur_sol[tj, 3])
                        tmp.append(cur_sol[tj, 4])
                        trace.append(tmp)

                    init_delta_array75 = [x*0.75 for x in init_delta_array]
                    init_delta_array50 = [x*0.5 for x in init_delta_array]

                    #k, gamma = self.Global_Discrepancy(init_delta_array75, traces)
                    tube100 = self.bloatToTube(k, gamma, init_delta_array, trace, dimensions)
                    tube75 = self.bloatToTube(k, gamma, init_delta_array75, trace, dimensions)
                    tube50 = self.bloatToTube(k, gamma, init_delta_array50, trace, dimensions)

                    safety100 = self.checkSafety(tube100)
                    safety75 = self.checkSafety(tube75)
                    safety50 = self.checkSafety(tube50)
                    if safety100 and safety75 and safety50:
                        #self.publish_command(cur_state[0][0])
                        #print('All Commands', cur_state[0])
                        #output[i] = p*old_trace[i+1] + (1-p)*new_trace[i]
                        # combined_trace = []
                        # len_trace = len(trace)
                        # len_prev_trace = len(self.prev_states)
                        # print(len_trace, len_prev_trace)
                        # for i in range(min(len_trace-1, len_prev_trace-1)):
                        #     new_point = [0] * dim_state
                        #     for j in range(dim_state):
                        #         new_point[j] = p*self.prev_states[i+1][j] + (1-p)*trace[i][j]
                        #     combined_trace.append(new_point)
                        # # if len_trace > len_prev_trace:
                        # #     combined_trace.append(trace[-1])
                        # #combined_trace.append(trace[-1])
                        # self.prev_states = combined_trace
                        # return combined_trace, tube100, tube75, tube50, True
                        return trace, tube100, tube75, tube50, True
                    
                else:  #update and store states for future use
                    states[j] = cur_state
                    sols[j] = cur_sol

        #self.publish_command("EMB")
        print ("Brake!")

        #output[i] = p*old_trace[i+1] + (1-p)*new_trace[i]
        # combined_trace = []
        # for i in range(len(trace)-1):
        #     new_point = [0] * dim_state
        #     for j in range(dim_state):
        #         new_point[j] = p * self.prev_states[i+1][j] + (1-p) * trace[i][j]
        #     combined_trace.append(new_point)
        # #combined_trace.append(trace[-1])
        # self.prev_states = combined_trace

        # return combined_trace, tube100, tube75, tube50, False
        return trace, tube100, tube75, tube50, False


    def bloatToTube(self, k, gamma, init_delta_array, trace, dimensions):
        center_trace = trace
        reach_tube = []
        trace_len = len(trace)
        for i in range(trace_len-1):
            #pdb.set_trace()
            time_interval = center_trace[i+1][0] - center_trace[0][0]
            lower_rec = [center_trace[i][0]]
            upper_rec = [center_trace[i+1][0]]

            for dim in range(1,dimensions):
                delta = k[dim-1] * math.exp(gamma[dim-1]*time_interval) * init_delta_array[dim-1]
                upper_rec.append(max(center_trace[i+1][dim],center_trace[i][dim])+delta)
                lower_rec.append(min(center_trace[i+1][dim],center_trace[i][dim])-delta)
            reach_tube.append(lower_rec)
            reach_tube.append(upper_rec)
        return reach_tube

    def read_data(self, traces):
        # Locals
        trace = traces[0]
        error_thred_time = 1e-6

        # Calculate variables
        self.dimensions = len(trace[0])
        self.dimensions_nt = self.dimensions - 1
        #trace_len = len(trace)
        #start_time = trace[0][0]
        self.end_time = trace[-1][0]

        # Align all the traces
        for i in range (len(traces)):
            initial_time = traces[i][0][0]
            for j in range (len(traces[i])):
                traces[i][j][0] = traces[i][j][0] - initial_time

        #reasign the start time and end time
        self.start_time = 0
        for i in range (len(traces)):
            self.end_time = min(self.end_time,traces[i][-1][0])

        #trim all the points after the end time
        traces_trim = []
        for i in range (len(traces)):
            trace_trim = []
            for j in range (len(traces[i])):
                if traces[i][j][0] <= self.end_time + error_thred_time:
                    trace_trim.append(traces[i][j])
            traces_trim.append(trace_trim)
            #print(len(trace_trim))

        #reasign trace_len
        self.trace_len = len(trace_trim)

        return traces_trim

    def doRectsInter(self, rect1, rect2):
        for i in range(len(rect1[0][:])):
            if rect1[0][i] > rect2[1][i] or rect1[1][i] < rect2[0][i]:
                return False
        return True

    # assuming that the unsafesets time is aligned with the time of the tube
    def checkSafety(self, tube):
        if tube:
            for id, unsafeset in self.pedestrian_tube_dict.items():
                for i in range(0,len(tube)-1,2):
                    for j in range(0,len(unsafeset)-1,2):
                        #print("Unsafe set:", [unsafesets[j], unsafesets[j+1]])
                        #print("tube: ", [tube[i][:3], tube[i+1][:3]])
                        if self.doRectsInter([unsafeset[j][1:], unsafeset[j+1][1:]], [tube[i][1:3], tube[i+1][1:3]]):
                            return False
        return True
    
    def randomPoint(self,lower, upper):
        """
        Pick a random point between lower and upper bound
        This function supports both int or list
        
        Args:
            lower (list or int or float): lowerbound.
            upper (list or int or float): upperbound.
        Returns:
            random point (either float or list of float)
        """
        if isinstance(lower, int) or isinstance(lower, float):
            return random.uniform(lower, upper)

        if isinstance(lower, list):
            assert len(lower) == len(upper), "Random Point List Range Error"

        return [random.uniform(lower[i], upper[i]) for i in range(len(lower))]


if __name__ == '__main__':
    #pid_controller = PID_controller(st_p=5.7, st_i=0.0, st_d=3.5, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=10.0)
    #pid_controller = PID_controller(st_p=5.0, st_i=0.0, st_d=2.7, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=1.0)
    pid_controller = PID_controller(st_p=8.8, st_i=0.5, st_d=5.8, sp_p=1.30, sp_d=0.02, sp_i=0.40, desired_speed=0.6)
    reachability_analyzer = reachability_analysis(controller=pid_controller)
    simple_monitor = simple_decision()
    
    print("Reachability node started!")
    # [sx, sy, delta, v, psi]1
    a = [3,3.5,4,4.5,5]
    b = [1,2,3,4,5,11,12]
    #time_bound = 5
    result_list = []
    # plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    color_reach = 'c'
    color_reach75 = 'g'
    color_reach50 = 'b'
    color_ped = 'r'
    color_lane = 'grey'
    data = np.loadtxt('sparse_lane_data.dat')#('lane_data.dat')
    smoothed_x = data[:, 0]-7
    smoothed_y = data[:, 1]+8

    for num_case in b:
        # update the pedestrain trajectory
        #ped_traj = pickle.load(open("data/ped_traj/predicted_ped_traj_"+str(i)+".pkl", "rb"))
        ped_traj = pickle.load(open("data/ped_traj_2/predicted_traj_"+str(num_case)+".pkl", "rb"))
        #print (ped_traj)
        reachability_analyzer.update_pedestrian(ped_traj)

        #update the car status 
        #car_states = pickle.load(open("data/data_with_steering/"+str(i)+".pkl", "rb" ))
        car_states = pickle.load(open("data/car_state/t"+str(num_case)+".pkl", "rb" ))
        print (car_states)

        #measure the accuracy
        time_interval = 0.0125
        car_trace = []
        #car_pos = set()
        for car_state in car_states:
            car_trace.append((car_state[-2][0], car_state[0][0], car_state[0][1]))
            #car_pos.add(car_state[0][0], car_state[0][1])
        car_trace.sort(key = lambda x: x[0])

        ####
        for time_bound in a:
            total_time = 0
            count = 0
            time_interval = 0.0125
            ped_interval = 0.5
            total_points = 0
            correct_points = [0,0,0]
            for n in range(len(car_states)):
                car_state = car_states[n]
                car_time = car_state[-2][0]
                ped_idx_start = min(int(car_time/ped_interval), len(ped_traj))
                ped_idx_end = min(int((car_time+time_bound)/ped_interval)+2, len(ped_traj))
                reachability_analyzer.update_pedestrian(ped_traj[ped_idx_start:ped_idx_end])
                reachability_analyzer.update_car_status(car_state)
                #print (ped_idx_start, ped_idx_end, car_time)
                start = time.time()
                #print(car_state[0])
                trace, tube100, tube75, tube50, safe = reachability_analyzer.Car_simulate(time_bound)
                #print (trace)

                # ax.clear()
                # plt.rcParams["font.family"]="Times New Roman"
                # plt.rcParams.update({'font.size': 12})
                # currentAxis = plt.gca()

                lane_trace = np.hstack((np.zeros((smoothed_x.shape[0],1)),smoothed_x.reshape((smoothed_x.shape[0],1)),smoothed_y.reshape((smoothed_y.shape[0],1))))
                #print("lane trace shape: ",smoothed_y.shape)
                lane_tube = reachability_analyzer.bloatToTube([1,1],[1,1],[1,1],lane_trace,3)
                
                if lane_tube:
                    for i in range(0, len(lane_tube)-1, 2):
                        w = lane_tube[i+1][1] - lane_tube[i][1]
                        h = lane_tube[i+1][2] - lane_tube[i][2]
                        rect = Rectangle((lane_tube[i][1],lane_tube[i][2]),w,h,linewidth=1,edgecolor=color_lane,facecolor=color_lane, alpha=0.1)
                        if i == 0:
                            lane_rect_label = rect
                        #rect.set_label('pedestrian trajectory')
                        # add the patch to axes
                        #currentAxis.add_patch(rect)
                if trace:
                    #print (trace)
                    #print (np.array(trace).size)
                    x = np.array(trace)[:,1]
                    y = np.array(trace)[:,2]
                else:
                    x, y = [], []
                #trace_plt, lane_plt = ax.plot(x, y, 'k.', smoothed_x, smoothed_y, 'y--')
                #car_plot = ax.plot([row[1] for row in car_trace], [row[2] for row in car_trace])
                #point_plot = ax.plot(car_trace[][])
                #print("Safety:", safe)
                #car_trace_idx = 0
                if tube100 and tube75 and tube50:
                    for i in range(0, len(tube100)-1, 2):
                        w100 = tube100[i+1][1] - tube100[i][1]
                        h100 = tube100[i+1][2] - tube100[i][2]
                        w75 = tube75[i+1][1] - tube75[i][1]
                        h75 = tube75[i+1][2] - tube75[i][2]
                        w50 = tube50[i+1][1] - tube50[i][1]
                        h50 = tube50[i+1][2] - tube50[i][2]
                        #left bottom corner
                        rect100 = Rectangle((tube100[i][1],tube100[i][2]),w100,h100,linewidth=1,edgecolor=color_reach,facecolor='none', alpha=0.5)
                        rect75 = Rectangle((tube75[i][1],tube75[i][2]),w75,h75,linewidth=1,edgecolor=color_reach75,facecolor='none', alpha=0.75)
                        rect50 = Rectangle((tube50[i][1],tube50[i][2]),w50,h50,linewidth=1,edgecolor=color_reach50,facecolor='none')

                        #measure accuracy
                        # cur_timestamp = car_time + i*time_interval
                        # low, high = 0, len(car_trace)-1
                        # match_flag = False
                        # while(low<=high):
                        #     mid = round((low + high) / 2)
                        #     if cur_timestamp < car_trace[mid][0]:
                        #         high = mid -1
                        #     elif cur_timestamp > car_trace[mid][0]:
                        #         low = mid + 1
                        #     else:
                        #         cur_x, cur_y = car_trace[mid][1], car_trace[mid][2]
                        #         match_flag = True
                        #         break

                        # if not match_flag:
                        #     if (car_trace[high][0]-cur_timestamp) > (cur_timestamp-car_trace[low][0]):
                        #         cur_x, cur_y = car_trace[low][1], car_trace[low][2]
                        #     else:
                        #         cur_x, cur_y = car_trace[high][1], car_trace[high][2]
                        
                        #print (cur_x, cur_y, cur_timestamp,w100,h100,(tube100[i][1], tube100[i][2])) 
                        #total_points += 1
                        # if ((cur_x-tube100[i][1]) < w100) and ((cur_y-tube100[i][2]) < h100):
                        #     correct_points += 1  
                        ####
                        [bx0, by0] = tube100[i][1:3]
                        [bx1, by1] = tube75[i][1:3]
                        [bx2, by2] = tube50[i][1:3]
                        if bx2 + w50/2.0 > -5:
                            total_points += 1
                            flag0, flag1, flag2 = False, False, False
                            for car_state in car_trace:
                                if ((car_state[1]-bx2)<w50) and ((car_state[1]-bx2)>0) and \
                                    ((car_state[2]-by2)<h50) and ((car_state[2]-by2)>0):
                                    flag0, flag1, flag2 = True, True, True
                                    break
                                elif ((car_state[1]-bx1)<w75) and ((car_state[1]-bx1)>0) and \
                                    ((car_state[2]-by1)<h75) and ((car_state[2]-by1)>0):
                                    flag1, flag0 = True, True
                                elif ((car_state[1]-bx0)<w100) and ((car_state[1]-bx0)>0) and \
                                    ((car_state[2]-by0)<h100) and ((car_state[2]-by0)>0):
                                    flag0 = True
                            if flag0:
                                correct_points[0] += 1
                            if flag1:
                                correct_points[1] += 1
                            if flag2:
                                correct_points[2] += 1

                        ####

                        # add the patch to axes
                        # currentAxis.add_patch(rect100)
                        # currentAxis.add_patch(rect75)
                        # currentAxis.add_patch(rect50)
                        if i == 0:
                            car_rect_label100 = rect100
                            car_rect_label75 = rect75
                            car_rect_label50 = rect50

                cur_time = time.time()-start
                count += 1.0
                total_time += cur_time
                if n == len(car_states)-1:
                    print("Average runing time:", total_time/count, "Accuracy:",correct_points[0]/total_points,correct_points[1]/total_points,correct_points[2]/total_points, ' ',num_case, time_bound)

                    result_list.append((total_time/count, correct_points[0]/total_points, correct_points[1]/total_points, correct_points[2]/total_points))
                    with open('result.data', 'wb') as filehandle:
                        # store the data as binary data stream
                        pickle.dump(result_list, filehandle)
                #trace_plt.set_label('a')
                #lane_plt.set_label('b')
                # plt.xlim((-10, 10))
                # plt.ylim((-10, 10))
                # plt.ylabel('y (m)', fontsize=15)
                # plt.xlabel('x (m)',fontsize=15)
                # plt.grid('on')

                # pedestrian_tube = None
                # if reachability_analyzer.pedestrian_trajectory.shape[0] > 0:
                pedestrian_tubes = reachability_analyzer.get_pedestrian_tubes()
                for id, pedestrian_tube in pedestrian_tubes.items():
                    for i in range(0, len(pedestrian_tube)-1, 2):
                        w = pedestrian_tube[i+1][1] - pedestrian_tube[i][1]
                        h = pedestrian_tube[i+1][2] - pedestrian_tube[i][2]
                        rect = Rectangle((pedestrian_tube[i][1],pedestrian_tube[i][2]),w,h,linewidth=1,edgecolor=color_ped,facecolor='none')
                        if i == 0:
                            ped_rect_label = rect
                        #rect.set_label('pedestrian trajectory')
                        # add the patch to axes
                        # currentAxis.add_patch(rect)

                # if pedestrian_tubes and not tube100:
                #     plt.legend((trace_plt, lane_plt, ped_rect_label), ('Predicted Car trajectory','Lane center','predicted pedestrian trajectory'))
                # elif tube100 and tube75 and tube50 and not pedestrian_tubes:
                #     plt.legend((trace_plt, lane_plt, car_rect_label100, car_rect_label75, car_rect_label50),\
                #         ('Predicted Car trajectory','Lane center','High confidence car reachtube', 'Medium confidence car reachtube','Low confidence car reachtube'))
                # elif tube100 and tube75 and tube50 and pedestrian_tubes:
                #     plt.legend((trace_plt, lane_plt, ped_rect_label, car_rect_label100, car_rect_label75, car_rect_label50),\
                #         ('Predicted Car trajectory','Lane center', 'predicted pedestrian trajectory', 'High confidence car reachtube', \
                #             'Medium confidence car reachtube','Low confidence car reachtube'))
                # else:
                #     plt.legend((trace_plt, lane_plt), ('Predicted Car trajectory','Lane center'))
                #plt.pause(0.05)
    #plt.show()
    print (result_list)

