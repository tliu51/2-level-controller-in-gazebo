import numpy as np


data = np.loadtxt('eceb_waypts_2.dat')
data_x = list(data[:, 0])
data_y = list(data[:, 1])
new_x = []
new_y = []

for i in range(len(data_x)-1):
    insert_x = (data_x[i] + data_x[i+1]) / 2.0
    insert_y = (data_y[i] + data_y[i+1]) / 2.0
    new_x.append(data_x[i])
    new_x.append(insert_x)
    new_y.append(data_y[i])
    new_y.append(insert_y)

data = np.column_stack((np.array(new_x), np.array(new_y)))
np.savetxt('eceb_3.dat', data)