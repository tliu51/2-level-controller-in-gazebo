"""
Written by Tianqi Liu, 2020 Feb.

It finds the optimal path for a car using bicycle model. 
"""

import copy
import heapq
import math
import matplotlib.pyplot as plt
import numpy as np
import time

show_animation = False

#possible steering controls
possible_str = {
    'l': -10,
    'l+': -22,
    'r+': +22,
    'r': +10,
    's': 0
}

#possible speed controls
possible_sp = {
    'f': 1,
    'b': -1
}


# total cost f(n) = actual cost g(n) + heuristic cost h(n)
class hybrid_a_star:
    def __init__(self, min_x, max_x, min_y, max_y, \
            obstacle=[], resolution=1, vehicle_length=2):

        ##TODO
        # car state = (x,y)
        # state tuple (f,g,(x,y), [(x1,y1),(x2,y2)...])
        # In dictionary, key = car_state, value = state tuple
        self.open_node = {}
        self.close_node = set()
        self.heap_queue = []

        # Define restricted space
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y

        #[(x, y), (...)...]
        self.obstacle = set(obstacle)
        self.resolution = resolution
        self.vehicle_length = vehicle_length

    def update_obstacle(self, new_obstacles, union=True):
        if union:
            self.obstacle.union(set(new_obstacles))
        else:
            self.obstacle = set(new_obstacles)

    def verify_node(self, new_discret_position): #discret x,y,theta
        x, y = new_discret_position[0], new_discret_position[1]
        if x <= self.min_x or x >= self.max_x or y <= self.min_y or y >= self.max_y:
            return False
        elif new_discret_position in self.close_node:
            return False
        elif (x,y) in self.obstacle: 
            return False
        return True

    def Euclid_distance(self, x1, y1, x2, y2):
        return math.sqrt((x1-x2)**2 + (y1-y2)**2)       

    #bicycle model
    def cal_new_node_bike(self, state, cur_str, cur_sp):
        # Everything need to be in continuous space
        # Those x, y are the center's
        # approximate the v to be the front wheel speed
        x, y, theta = float(state[0]), float(state[1]), float(state[2])
        delta = cur_str * math.pi / 180.0
        theta = theta * math.pi / 180.0

        #calculate the exact position for front and rear wheels
        x_f = x + 0.5*self.vehicle_length*math.cos(theta)
        x_r = x - 0.5*self.vehicle_length*math.cos(theta)
        y_f = y + 0.5*self.vehicle_length*math.sin(theta)
        y_r = y - 0.5*self.vehicle_length*math.sin(theta)
        v_f = cur_sp
        v_r = v_f * math.cos(delta)

        x_r = x_r + v_r*math.cos(theta)
        y_r = y_r + v_r*math.sin(theta)
        x_f = x_f + v_f*math.cos(theta+delta)
        y_r = y_r + v_f*math.sin(theta+delta)

        #new continuous state for car 
        theta = theta + v_f * math.sin(delta) / self.vehicle_length
        if theta > math.pi:
            theta = -2*math.pi + theta
        elif theta < -math.pi:
            theta = 2*math.pi + theta
        x = (x_r + x_f) / 2
        y = (y_r + y_f) / 2
        
        theta = theta * 180 / math.pi

        if cur_sp >= 0:
            sp_cost = cur_sp
        else:
            sp_cost = abs(cur_sp) * 3

        cost = abs(delta) + sp_cost
        return (x,y,theta), cost

####

    """
    For each node n, we need to store:
    (discret_x, discret_y, heading angle theta), 
    (continuous x, continuous y, heading angle theta)
    cost g, f,
    path [(continuous x, continuous y),...]

    The full node is stored in dictionary open node.
    (discret_x, discret_y, heading angle theta) is the key;
    ((continuous x, continuous y, heading angle theta),
    g, f, path [(continuous x, continuous y),...]) is the value
    
    In heap queue we only need to store the total cost f, 
    and discret (x,y,theta); We use it to keep track of the node 
    with minimal estimated total cost  

    For closed nodes we only need to store the discret (x,y,theta) in a set.

    start: discret (x, y, theta)
    end: discret (x, y, theta)
    """
    def find_path(self, start, end):

        ##TODO

        sol_path = []
        min_cost = float('inf')
        dist = self.Euclid_distance(start[0],start[1],end[0],end[1])
        #set start point
        #for each node 
        #(f,g,(discret_x,discret_y,heading angle theta), path [(x,y,theta),...])
        heapq.heapify(self.heap_queue)
        heapq.heappush(self.heap_queue, (dist, start))
        self.open_node[start] = (start, 0, dist, [start])
        #self.close_node.add(start)

        #iterate until a path to target is found
        while(self.heap_queue):
            prev_discret_state = heapq.heappop(self.heap_queue)
            prev_discret_position = prev_discret_state[1]
            #get the full node from open node dictionary
            if prev_discret_position not in self.open_node or prev_discret_position in self.close_node:
                continue
            prev_node = self.open_node.pop(prev_discret_position)
            prev_position = prev_node[0]
            prev_g = prev_node[1]
            prev_path = prev_node[3]
            #plt.plot(prev_position[0], prev_position[1], "xc")
            #plt.pause(0.0001)

            #update solution if reach the goal with lowest cost
            if prev_discret_position == end:
                #if prev_g < min_cost:
                min_cost = prev_g
                sol_path = prev_path
                #print(sol_path)
                return sol_path

            for str_command, cur_str in possible_str.items():
                for sp_command, cur_sp in possible_sp.items():
                    new_position, cost = self.cal_new_node_bike(prev_position,cur_str,cur_sp)
                    new_discret_position = (round(new_position[0]),round(new_position[1]),round(new_position[2]))

                    if self.verify_node(new_discret_position):
                        new_h = self.Euclid_distance(new_position[0],new_position[1],end[0],end[1])
                        new_g = prev_g + cost
                        new_f = new_h + new_g
                        new_path = copy.deepcopy(prev_path)
                        new_path.append(new_position)
                        
                        if new_discret_position in self.open_node:
                            if new_g < self.open_node[new_discret_position][1]:
                                self.open_node[new_discret_position] = (new_position, new_g, new_f, new_path)
                                heapq.heappush(self.heap_queue, (new_f, new_discret_position))
                        else:
                            heapq.heappush(self.heap_queue, (new_f, new_discret_position))
                            self.open_node[new_discret_position] = (new_position, new_g, new_f, new_path)
                    else:
                        self.close_node.add(new_discret_position)
            self.close_node.add(prev_discret_position)

        ####
        # return a list of continuous states as way points 
        return sol_path 


def main():
    print(__file__ + " start!!")

    # start and goal position
    #(x, y, theta) in meters, meters, degrees 
    sx, sy, stheta= -5, -5, 0
    gx, gy, gtheta = 0, -5, 90

    #create obstacles
    obstacle = []
    # for i in range(30):
    #     obstacle.append((i-15, -15))
    #     obstacle.append((i-14, 15))
    #     obstacle.append((-15, i-14))
    #     obstacle.append((15, i-15))

    for i in range(2):
        obstacle.append((0,i))
        obstacle.append((0,-i))    

    ox, oy = [], []
    for (x,y) in obstacle:
        ox.append(x)
        oy.append(y)

    if show_animation:  # pragma: no cover
        plt.plot(ox, oy, ".k")
        plt.plot(sx, sy, "xr")
        plt.plot(gx, gy, "xb")
        plt.grid(True)
        plt.axis("equal")

    hy_a_star = hybrid_a_star(-20, 20, -20, 20, obstacle=obstacle, \
        resolution=1, vehicle_length=2)
    start_time = time.time()
    path = hy_a_star.find_path((sx,sy,stheta), (gx,gy,gtheta))
    print (time.time() - start_time)
    print(path)
    path_dist = 0
    for i in range(len(path)-1):
        path_dist += np.sqrt((path[i][0]-path[i+1][0])**2 + (path[i][1]-path[i+1][1])**2)

    print (path_dist)

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    if show_animation:  # pragma: no cover
        plt.plot(rx, ry, "-r")
        plt.show()


if __name__ == '__main__':
    show_animation = True
    main()