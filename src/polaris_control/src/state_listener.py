import rospy
from gazebo_msgs.msg import ModelState
from std_msgs.msg import Int32, Float64, String, Float64MultiArray

class stateListener():
    def __init__(self):
        self.state = []
        self.waypointSub = rospy.Subscriber("/car_state", Float64MultiArray, \
            self.set_state, queue_size=1)

    def set_state(self, data):
        self.state = data.data
        #print ("get data:", data.data)

    def get_state(self):
        return self.state
    

