#!/usr/bin/env python

#from decawave.msg import position, PositionWithSpeed
#from pii.msg import trajectory
import numpy as np
import math
from math import cos, atan, pi, sqrt
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.animation as animation
from matplotlib import style
import rospy
from std_msgs.msg import String, Bool, Float32, Float64, Char, UInt64, Float64MultiArray
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from scipy.integrate import odeint
import scipy as scipy
import scipy.special as sc
import warnings
import time 
from statistics import mean 
import cvxpy
import cvxopt
import random
#import glpk
import subprocess
# from k_gamma_calc import k_gamma_calc
import copy
#from GetOffset import OffsetCalculator
from NewPIDController import PID_controller
from decawave.msg import trajectory
import pickle

reachability_pub = rospy.Publisher('/gem_reachability', Char, queue_size=1)
control_pub = rospy.Publisher('/gem_decision', Char, queue_size=1)

# Ratio between physical wheel angle to steering wheel angle
global wheel_to_steering_ratio
wheel_to_steering_ratio = 0.0506145
wheel_radius = 0.32
wheel_inertia = 1.0
max_steering_angle = 0.4 #(max_wheel*wheel_to_steering_ratio)
global car_state_position
car_state_position = 3
'''
this decision module checks that based on current vehicle status, 
whether the vechicle will collide with dynamic obstacles
'''

class simple_decision:
    def __init__(self, time_bound):
        self.car_update_time = 0
        self.look_ahead_time = time_bound
        self.safe_distance = 1.5

        #car dimension
        self.car_length = 3

        #car status
        self.car_x = 0
        self.car_prev_x = []
        self.car_y = 0
        self.car_prev_y = []
        self.car_theta = 0
        self.car_prev_thetas = []
        self.car_delta = 0
        self.car_prev_deltas = []
        self.car_v = 0
        self.car_prev_v = [] 
        self.car_offset = 0
        self.car_prev_offset = []

        #pedestrian status
        #each pedestrian has unique id
        self.pedestrian_trajectory_dict = {}
        self.pedestrian_tube_dict = {}
        self.pedestrian_tube = None

        #store states
        self.prev_states = []

        #store lane
        self.lane_x = None
        self.lane_y = None
    
    #smooth out the inputs to reduce noise
    def update_car_status(self, car_inputs):
        self.car_update_time = car_inputs[-2][0]
        # self.car_x = car_inputs[0][0]
        # self.car_y = car_inputs[0][1]
        carv_x, carv_y = car_inputs[-4][0], car_inputs[-4][1]
        self.car_v = np.sqrt(carv_x**2 + carv_y**2)

        self.car_prev_x.append(car_inputs[0][0])
        if len(self.car_prev_x) > 3:
            self.car_prev_x.pop(0)
        self.car_x = mean(self.car_prev_x)

        self.car_prev_y.append(car_inputs[0][1])
        if len(self.car_prev_y) > 3:
            self.car_prev_y.pop(0)
        self.car_y = mean(self.car_prev_y)

        self.car_prev_thetas.append(math.atan2(carv_x,carv_y))
        if len(self.car_prev_thetas) > 3:
            self.car_prev_thetas.pop(0)
        self.car_theta = mean(self.car_prev_thetas)

        if car_inputs[-1][0] == None:
            self.car_prev_deltas.append(0)
        else:
            self.car_prev_deltas.append(car_inputs[-1][0])
        if len(self.car_prev_deltas) > 3:
            self.car_prev_deltas.pop(0)
        self.car_delta = mean(self.car_prev_deltas)

    #store pedestrian trajectory in the form of [(time,x,y), ...]
    def update_pedestrian(self, pedestrian_inputs):
        self.pedestrian_start_ts = 0

        pedestrian_traj = []
        for i in range(len(pedestrian_inputs)):
            pedestrian_traj.append([self.pedestrian_start_ts+i*0.5, pedestrian_inputs[i][0], pedestrian_inputs[i][1]])
        pedestrian_traj = np.array(pedestrian_traj)
        #print (pedestrian_traj)

        self.pedestrian_trajectory_dict[0] = pedestrian_traj
        self.pedestrian_tube_dict[0] = self.bloatToTube([0,0],[0,0],[0,0],pedestrian_traj,3)

    def update_lanes(self, lane_x, lane_y):
        self.lane_x = lane_x
        self.lane_y = lane_y

    def get_pedestrian_tubes(self):
        return self.pedestrian_tube_dict

    #for gazebo only
    def gazebo_car_satus(self, data):
        car_x = data.pose[car_state_position].position.x  # vehicle x position
        car_y = data.pose[car_state_position].position.y  # vehicle y position
        # calculate heading angle
        dx = car_x - self.car_x
        dy = car_y - self.car_y
        self.car_x, self.car_y = car_x, car_y
        #print (car_x, car_y)
        self.car_theta = math.atan2(dx,dy)

    def gazebo_car_speed(self, data):
        self.car_v = (data.velocity[4] + data.velocity[5])*0.5

    def gazebo_car_steering(self, data):
        self.car_delta = data.data

    def gazebo_update_pedestrian(self, pedestrian_inputs):
        pedestrian_id = pedestrian_inputs.x[0]
        self.pedestrian_start_ts = pedestrian_inputs.time

        pedestrian_traj = []
        for i in range(1, len(pedestrian_inputs.x)):
            pedestrian_traj.append([int(self.pedestrian_start_ts/1000 + i*0.1),\
                 pedestrian_inputs.x[i], pedestrian_inputs.y[i]])
        pedestrian_traj = np.array(pedestrian_traj)

        self.pedestrian_trajectory_dict[pedestrian_id] = pedestrian_traj
        self.pedestrian_tube_dict[pedestrian_id] = self.bloatToTube([0,0],[0,0],[0,0],pedestrian_traj,3)

    '''
    This function check the safety and 
    return the predictive vehicle trajectory for the next look ahead time
    and a list of points that are "unsafe"
    in the form of [(x,y),...],
    '''
    def check_safety(self):
        predict_traj = []
        safe = True

        # assume vehicle follow the lane perfectly
        node = 0
        ref_states = list(zip(self.lane_x,self.lane_y))

        # change tuple to list
        for i in range(len(ref_states)):
            ref_states[i] = list(ref_states[i])

        # find the nearest neighbour
        car_states = (self.car_x, self.car_y)
        for i in range(len(ref_states)):
            if self.GetDistance(ref_states[i], car_states) < self.GetDistance(ref_states[node], car_states):
                node = i

        # find all way points for next look ahead time
        car_points = []
        t_count = 0
        while (t_count < self.look_ahead_time):
            car_points.append(ref_states[node])
            node += 1
            if node >= len(self.lane_x):
                segment_dist = self.GetDistance(ref_states[node-1], ref_states[0])
                node = 0
            else:
                segment_dist = self.GetDistance(ref_states[node-1], ref_states[node]) 
            t_segment = segment_dist / self.car_v
            t_count += t_segment
            

        # check if in safe distance
        unsafe_points = []
        for predict_traj in self.pedestrian_trajectory_dict.values():
            for car_point in car_points:
                for ped_point in predict_traj:
                    if abs(self.GetDistance(car_point, ped_point[1:3])) < self.safe_distance:
                        safe = False
                        unsafe_points.append(car_point)
                        break

        return car_points, unsafe_points#, safe

    # get the distance between two points
    def GetDistance(self, p_1, p_2):
        dist = sqrt(pow((p_1[0] - p_2[0]), 2) + pow((p_1[1] - p_2[1]), 2))
        return dist

    # get the distance between a point and a line
    def GetDistance2(self, point, k, b):
        dist = (-k*point[0] + point[1] - b) / math.sqrt(k**2 + 1)
        return dist

    def bloatToTube(self, k, gamma, init_delta_array, trace, dimensions):
        center_trace = trace
        reach_tube = []
        trace_len = len(trace)
        for i in range(trace_len-1):
            #pdb.set_trace()
            time_interval = center_trace[i+1][0] - center_trace[0][0]
            lower_rec = [center_trace[i][0]]
            upper_rec = [center_trace[i+1][0]]

            for dim in range(1,dimensions):
                delta = k[dim-1] * math.exp(gamma[dim-1]*time_interval) * init_delta_array[dim-1]
                upper_rec.append(max(center_trace[i+1][dim],center_trace[i][dim])+delta)
                lower_rec.append(min(center_trace[i+1][dim],center_trace[i][dim])-delta)
            reach_tube.append(lower_rec)
            reach_tube.append(upper_rec)
        return reach_tube

if __name__ == '__main__':
    time_bound = 3
    num_case = 12 #test case number, choose from [1,2,3,4,5,11,12]

    decision_module = simple_decision(time_bound)
    
    print("Reachability node started!")
    # [sx, sy, delta, v, psi]1
    # a = [3,3.5,4,4.5,5]
    # b = [1,2,3,4,5,11,12]
    result_list = []
    # plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    color_reach = 'c'
    color_reach75 = 'g'
    color_reach50 = 'b'
    color_ped = 'r'
    color_lane = 'grey'
    plt.xlim((0, 30))
    plt.ylim((0, 15))
    plt.ylabel('y (m)', fontsize=15)
    plt.xlabel('x (m)',fontsize=15)
    plt.grid('on')

    with open('obstacle.data', 'rb') as filehandle:
        # read the data as binary data stream
        obstacle = pickle.load(filehandle)
    
    ox, oy = [], []
    # for (x,y) in obstacle:
    #     ox.append(x)
    #     oy.append(y)

    data = np.loadtxt('sparse_lane_data.dat')#('eceb_4.dat')#
    lane_x = data[:, 0]-7
    lane_y = data[:, 1]+8
    decision_module.update_lanes(lane_x, lane_y)
    lane_trace = np.hstack((np.zeros((lane_x.shape[0],1)),lane_x.reshape((lane_x.shape[0],1)),lane_y.reshape((lane_y.shape[0],1))))
    #print("lane trace shape: ",lane_y.shape)
    lane_tube = decision_module.bloatToTube([1,1],[1,1],[1,1],lane_trace,3)

    car_position = np.loadtxt('gazebo_car_position.dat')#('gazebo_car_position_2.dat')
    car_x = car_position[:, 0]
    car_y = car_position[:, 1]
    car_real_trace = np.hstack((np.zeros((car_x.shape[0],1)),car_x.reshape((car_x.shape[0],1)),car_y.reshape((car_y.shape[0],1))))    

    rospy.init_node('Simple_Decision')
    rospy.Subscriber("/gazebo/model_states", ModelStates, decision_module.gazebo_car_satus, queue_size=1)
    rospy.Subscriber("/polaris/joint_states", JointState, decision_module.gazebo_car_speed, queue_size=1)
    rospy.Subscriber("/polaris/front_left_steering_position_controller/command", Float64, decision_module.gazebo_car_steering, queue_size=1)
    rospy.Subscriber("/predicted_trajectory",trajectory, decision_module.gazebo_update_pedestrian, queue_size=1)

    # update the pedestrain trajectory
    #ped_traj = pickle.load(open("data/ped_traj_2/predicted_traj_"+str(num_case)+".pkl", "rb"))
    #print (ped_traj)
    #decision_module.update_pedestrian(ped_traj)

    #update the car status 
    #car_states = pickle.load(open("data/data_with_steering/"+str(i)+".pkl", "rb" ))
    #car_states = pickle.load(open("data/car_state/t"+str(num_case)+".pkl", "rb" ))
    def runtime(i):
        if not rospy.core.is_shutdown():
            #print (ped_idx_start, ped_idx_end, car_time)
            start = time.time()
            #print(car_state[0])
            trace, unsafe_trace = decision_module.check_safety()
            #print (trace)

            ax.clear()
            currentAxis = plt.gca()
            # plt.xlim((0, 30))
            # plt.ylim((0, 15))
            plt.xlim((-10, 10))
            plt.ylim((-10, 10))

            if lane_tube:
                for i in range(0, len(lane_tube)-1, 2):
                    w = lane_tube[i+1][1] - lane_tube[i][1]
                    h = lane_tube[i+1][2] - lane_tube[i][2]
                    rect = Rectangle((lane_tube[i][1],lane_tube[i][2]),w,h,linewidth=3,edgecolor=color_lane,facecolor=color_lane, alpha=0.3)
                    if i == 0:
                        lane_rect_label = rect
                    rect.set_label('pedestrian trajectory')
                    # add the patch to axes
                    currentAxis.add_patch(rect)

            x, y = [], []
            if trace:
                #print (trace)
                #print (np.array(trace).size)
                x = np.array(trace)[:,0]
                y = np.array(trace)[:,1]

            unsafe_x, unsafe_y = [], []
            if unsafe_trace:
                #print (unsafe_trace)
                print("Brake")
                unsafe_x = np.array(unsafe_trace)[:,0]
                unsafe_y = np.array(unsafe_trace)[:,1]

            ax.plot(x, y, '-g', unsafe_x, unsafe_y, '-r', car_x, car_y, '^b', linewidth=4)
            ax.plot(lane_x, lane_y, 'y--', ox, oy, '.k')
            pedestrian_tubes = decision_module.get_pedestrian_tubes()
            for id, pedestrian_tube in pedestrian_tubes.items():
                for i in range(0, len(pedestrian_tube)-1, 2):
                    w = pedestrian_tube[i+1][1] - pedestrian_tube[i][1]
                    h = pedestrian_tube[i+1][2] - pedestrian_tube[i][2]
                    rect = Rectangle((pedestrian_tube[i][1],pedestrian_tube[i][2]),w,h,linewidth=2,edgecolor=color_ped,facecolor='none')
                    if i == 0:
                        ped_rect_label = rect
                    rect.set_label('pedestrian trajectory')
                    # add the patch to axes
                    currentAxis.add_patch(rect)
            #car_plot = ax.plot([row[1] for row in car_trace], [row[2] for row in car_trace])

            #print(time.time() - start)
            plt.draw()
    ani = animation.FuncAnimation(fig, runtime, frames=2)
    plt.show()
