import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.animation as animation
import math
import pickle
import rospy
import sys
import time


from hybrid_a_star import hybrid_a_star
from state_listener import stateListener
from reachability_analysis import reachability_analysis
from simple_decision import simple_decision
from sensor_msgs.msg import JointState
from gazebo_msgs.msg import ModelStates
from decawave.msg import trajectory
from std_msgs.msg import String, Bool, Float32, Float64, Char


environment = sys.argv[1]
g_x = float(sys.argv[2])
g_y = float(sys.argv[3])
g_theta = float(sys.argv[4])
decisionModule = sys.argv[5]

show_animation = True

# class main_controller():
#     def __init__(self, environment, gx, gy, gtheta, decisionModule):
        


if __name__ == '__main__':
    rospy.init_node("main_control", anonymous=True)
    myStateListener = stateListener()

    # Hybrid A* Path Planning
    obstacle = []
    if (environment == "highbay"):
        min_x, max_x, min_y, max_y = -15, 15, -15, 15

        # for i in range(3,8):
        #     for j in range(3,7):
        #         obstacle.append((j,i))
        #         obstacle.append((j,i))
        #         obstacle.append((j,i))
        #         obstacle.append((j,i))

        for i in range(-3,3):
            for j in range(-3,3):
                obstacle.append((j,i))
                obstacle.append((j,i))
                obstacle.append((j,i))
                obstacle.append((j,i))

        gx = round(g_x)  # [m]
        gy = round(g_y)  # [m]
        gtheta = round(g_theta)
    elif (environment == "eceb"):
        min_x, max_x, min_y, max_y = -100, 100, -100, 100

        with open('obstacle.data', 'rb') as filehandle:
        # read the data as binary data stream
            obstacle = pickle.load(filehandle)

        for i in range(3,8):
            for j in range(3,7):
                obstacle.append((j,i))
                obstacle.append((j,i))
                obstacle.append((j,i))
                obstacle.append((j,i))

        # gx = round(30) #(50)  # [m]
        # gy = round(0) #(80)  # [m]
        # gtheta = round(180)
        gx = round(g_x)  # [m]
        gy = round(g_y)  # [m]
        gtheta = round(g_theta)

    else:
        min_x, max_x, min_y, max_y = -100, 100, -100, 100

    curState = []
    while (curState == []):
        #### set the start of path planner ####
        print ("wait for car state")
        time.sleep(1)
        curState = myStateListener.get_state()

    sx = round(curState[0])  # [m]
    sy = round(curState[1])  # [m]
    stheta = round(curState[2] * 180 / math.pi)

    grid_size = 1.0

    # print((sx, sy, stheta), (gx,gy,gtheta))
    print("Environment is set up, start path planner")
    hybrid_a_star = hybrid_a_star(min_x, max_x, min_y, max_y, obstacle=obstacle, \
        resolution=1, vehicle_length=2)
    path = hybrid_a_star.find_path((sx,sy,stheta), (gx,gy,gtheta))

    # file = open('eceb_path_1.dat',"w")
    # file.write(path)
    # file.close()
    # np.savetxt('eceb_path_1.dat', path)
    # with open('eceb_path.data', 'wb') as filehandle:
    #     # store the data as binary data stream
    #     pickle.dump(obstacle_list, filehandle)
    print ("cur_path", sx, sy, gx, gy, path)

    ox, oy = [], []
    for (x,y) in obstacle:
        ox.append(x)
        oy.append(y)

    rx, ry = [], []
    for node in path:
        rx.append(node[0])
        ry.append(node[1])

    plt.plot(ox, oy, ".k")
    plt.plot(sx, sy, "xr")
    plt.plot(gx, gy, "xb")
    plt.plot(rx, ry, "-r")
    plt.grid(True)
    plt.axis("equal")
    plt.show()

    # #Decision Module
    # time_bound = 3
    # if decisionModule == "simple":
    #     decision_module = simple_decision(time_bound)
    # elif decisionModule == "reach":
    #     decision_module = reachability_analysis()

    # left_steer_pub = rospy.Publisher("/path_update", trajectory, queue_size=1)
    # rospy.Subscriber("/gazebo/model_states", ModelStates, decision_module.gazebo_car_satus, queue_size=1)
    # rospy.Subscriber("/polaris/joint_states", JointState, decision_module.gazebo_car_speed, queue_size=1)
    # rospy.Subscriber("/polaris/front_left_steering_position_controller/command", Float64, decision_module.gazebo_car_steering, queue_size=1)
    # rospy.Subscriber("/predicted_trajectory",trajectory, decision_module.update_pedestrian, queue_size=1)

    # fig = plt.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # color_reach = 'c'
    # color_reach75 = 'g'
    # color_reach50 = 'b'
    # color_ped = 'r'
    # color_lane = 'grey'

    # data = np.loadtxt('sparse_lane_data.dat')#('lane_data.dat')
    # smoothed_x = data[:, 0]-7
    # smoothed_y = data[:, 1]+8

    # lane_trace = np.hstack((np.zeros((smoothed_x.shape[0],1)),smoothed_x.reshape((smoothed_x.shape[0],1)),smoothed_y.reshape((smoothed_y.shape[0],1))))
    # #print("lane trace shape: ",smoothed_y.shape)
    # lane_tube = decision_module.bloatToTube([1,1],[1,1],[1,1],lane_trace,3)

    # def replan():
    #     curState = []
    #     while (curState == []):
    #         #### set the start of path planner ####
    #         print ("wait for car state")
    #         time.sleep(1)
    #         curState = myStateListener.get_state()

    #     sx = round(curState[0])  # [m]
    #     sy = round(curState[1])  # [m]
    #     stheta = round(curState[2] * 180 / math.pi)

    # def reach_runtime(i):
    #     if not rospy.core.is_shutdown():
    #         start = time.time()
    #         # unsafesets = [[[tmin1, xmin1, ymin1],[tmax1, xmax1, ymax1]], [[tmin2, xmin2, ymin2],[tmax2, xmax2, ymax2]]]
    #         # #print(np.array(trace)[:,1:3])

    #         trace, tube100, tube75, tube50, safe = decision_module.Car_simulate(time_bound)
    #         #print (trace)
    #         ax.clear()
    #         plt.rcParams["font.family"]="Times New Roman"
    #         plt.rcParams.update({'font.size': 12})
    #         currentAxis = plt.gca()
            
    #         if lane_tube:
    #             for i in range(0, len(lane_tube)-1, 2):
    #                 w = lane_tube[i+1][1] - lane_tube[i][1]
    #                 h = lane_tube[i+1][2] - lane_tube[i][2]
    #                 rect = Rectangle((lane_tube[i][1],lane_tube[i][2]),w,h,linewidth=1,edgecolor=color_lane,facecolor=color_lane, alpha=0.1)
    #                 if i == 0:
    #                     lane_rect_label = rect
    #                 #rect.set_label('pedestrian trajectory')
    #                 # add the patch to axes
    #                 currentAxis.add_patch(rect)
    #         if trace:
    #             x = np.array(trace)[:,1]
    #             y = np.array(trace)[:,2]
    #         else:
    #             x, y = [], []
    #         trace_plt, lane_plt = ax.plot(x, y, 'k.', smoothed_x, smoothed_y, 'y--')
    #         #print("Safety:", safe)
    #         if tube100 and tube75 and tube50:
    #             for i in range(0, len(tube100)-1, 2):
    #                 w100 = tube100[i+1][1] - tube100[i][1]
    #                 h100 = tube100[i+1][2] - tube100[i][2]
    #                 w75 = tube75[i+1][1] - tube75[i][1]
    #                 h75 = tube75[i+1][2] - tube75[i][2]
    #                 w50 = tube50[i+1][1] - tube50[i][1]
    #                 h50 = tube50[i+1][2] - tube50[i][2]
    #                 rect100 = Rectangle((tube100[i][1],tube100[i][2]),w100,h100,linewidth=1,edgecolor=color_reach,facecolor='none', alpha=0.5)
    #                 rect75 = Rectangle((tube75[i][1],tube75[i][2]),w75,h75,linewidth=1,edgecolor=color_reach75,facecolor='none', alpha=0.75)
    #                 rect50 = Rectangle((tube50[i][1],tube50[i][2]),w50,h50,linewidth=1,edgecolor=color_reach50,facecolor='none')

    #                 # add the patch to axes
    #                 currentAxis.add_patch(rect100)
    #                 currentAxis.add_patch(rect75)
    #                 currentAxis.add_patch(rect50)
    #                 if i == 0:
    #                     car_rect_label100 = rect100
    #                     car_rect_label75 = rect75
    #                     car_rect_label50 = rect50

    #         cur_time = time.time()-start
    #         #print("This time:", cur_time)
    #         #trace_plt.set_label('a')
    #         #lane_plt.set_label('b')
    #         plt.xlim((-10, 10))
    #         plt.ylim((-10, 10))
    #         plt.ylabel('y (m)', fontsize=15)
    #         plt.xlabel('x (m)',fontsize=15)
    #         plt.grid('on')

    #         pedestrian_tubes = decision_module.get_pedestrian_tubes()
    #         for id, pedestrian_tube in pedestrian_tubes.items():
    #             for i in range(0, len(pedestrian_tube)-1, 2):
    #                 w = pedestrian_tube[i+1][1] - pedestrian_tube[i][1]
    #                 h = pedestrian_tube[i+1][2] - pedestrian_tube[i][2]
    #                 rect = Rectangle((pedestrian_tube[i][1],pedestrian_tube[i][2]),w,h,linewidth=1,edgecolor=color_ped,facecolor='none')
    #                 if i == 0:
    #                     ped_rect_label = rect
    #                 #rect.set_label('pedestrian trajectory')
    #                 # add the patch to axes
    #                 currentAxis.add_patch(rect)

    #         if pedestrian_tubes and not tube100:
    #             plt.legend((trace_plt, lane_plt, ped_rect_label), ('Predicted Car trajectory','Lane center','predicted pedestrian trajectory'))
    #         elif tube100 and tube75 and tube50 and not pedestrian_tubes:
    #             plt.legend((trace_plt, lane_plt, car_rect_label100, car_rect_label75, car_rect_label50),\
    #                  ('Predicted Car trajectory','Lane center','High confidence car reachtube', 'Medium confidence car reachtube','Low confidence car reachtube'))
    #         elif tube100 and tube75 and tube50 and pedestrian_tubes:
    #             plt.legend((trace_plt, lane_plt, ped_rect_label, car_rect_label100, car_rect_label75, car_rect_label50),\
    #                   ('Predicted Car trajectory','Lane center', 'predicted pedestrian trajectory', 'High confidence car reachtube', \
    #                       'Medium confidence car reachtube','Low confidence car reachtube'))
    #         else:
    #             plt.legend((trace_plt, lane_plt), ('Predicted Car trajectory','Lane center'))

    #         plt.draw()

    # def simple_runtime(i):
    #     if not rospy.core.is_shutdown():
    #         trace, unsafe_trace = decision_module.check_safety()
    #         #print (trace)

    #         ax.clear()
    #         plt.rcParams["font.family"]="Times New Roman"
    #         plt.rcParams.update({'font.size': 12})
    #         plt.xlim((-10, 1))
    #         plt.ylim((-10, 10))
    #         plt.ylabel('y (m)', fontsize=15)
    #         plt.xlabel('x (m)',fontsize=15)
    #         plt.grid('on')
    #         currentAxis = plt.gca()

    #         if lane_tube:
    #             for i in range(0, len(lane_tube)-1, 2):
    #                 w = lane_tube[i+1][1] - lane_tube[i][1]
    #                 h = lane_tube[i+1][2] - lane_tube[i][2]
    #                 rect = Rectangle((lane_tube[i][1],lane_tube[i][2]),w,h,linewidth=1,edgecolor=color_lane,facecolor=color_lane, alpha=0.1)
    #                 if i == 0:
    #                     lane_rect_label = rect
    #                 rect.set_label('pedestrian trajectory')
    #                 # add the patch to axes
    #                 currentAxis.add_patch(rect)

    #         x, y = [], []
    #         if trace:
    #             #print (trace)
    #             #print (np.array(trace).size)
    #             x = np.array(trace)[:,0]
    #             y = np.array(trace)[:,1]

    #         unsafe_x, unsafe_y = [], []
    #         if unsafe_trace:
    #             print (unsafe_trace)
    #             unsafe_x = np.array(unsafe_trace)[:,0]
    #             unsafe_y = np.array(unsafe_trace)[:,1]

    #         trace_plt, lane_plt, unsafe_plt = ax.plot(x, y, '-k', lane_x, lane_y, 'y--', unsafe_x, unsafe_y, '-r')
    #         pedestrian_tubes = decision_module.get_pedestrian_tubes()
    #         for id, pedestrian_tube in pedestrian_tubes.items():
    #             for i in range(0, len(pedestrian_tube)-1, 2):
    #                 w = pedestrian_tube[i+1][1] - pedestrian_tube[i][1]
    #                 h = pedestrian_tube[i+1][2] - pedestrian_tube[i][2]
    #                 rect = Rectangle((pedestrian_tube[i][1],pedestrian_tube[i][2]),w,h,linewidth=1,edgecolor=color_ped,facecolor='none')
    #                 if i == 0:
    #                     ped_rect_label = rect
    #                 #rect.set_label('pedestrian trajectory')
    #                 # add the patch to axes
    #                 currentAxis.add_patch(rect)
    #         #car_plot = ax.plot([row[1] for row in car_trace], [row[2] for row in car_trace])

    #         plt.draw()

    # if decisionModule == "simple":
    #     ani = animation.FuncAnimation(fig, simple_runtime, frames=120)
    # elif decisionModule == "reach":
    #     ani = animation.FuncAnimation(fig, reach_runtime, frames=120)
    # plt.show()