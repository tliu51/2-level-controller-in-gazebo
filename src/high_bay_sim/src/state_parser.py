#!/usr/bin/env python

import rospy
import copy
import re
import time
import numpy as np
import math
import pickle
import os
import signal


from std_msgs.msg import Int32, Float64, String, Float64MultiArray
from geometry_msgs.msg import Twist, Pose, Point
from gazebo_msgs.srv import GetModelState, GetModelStateResponse
from gazebo_msgs.msg import ModelStates, LinkStates, LinkState, ModelState
from rosgraph_msgs.msg import Clock
from decawave.msg import position
from decawave.msg import trajectory




global collected_data, timestamp, steering_command
collected_data = []
timestamp = None
steering_command = None



def clock_cb(data):
    global timestamp
    timestamp = data.clock.secs + data.clock.nsecs/1000000000.
    print(timestamp)

def steering_command_cb(data):
    global steering_command
    steering_command = data.data



class state_parser:

    def __init__(self, ped_goals):
        self.ped_indicies = None
        self.car_index = None


    def model_state_cb(self, data):
        global timestamp, steering_command, collected_data

        current_dp = []

        if self.ped_indicies == None or self.car_index == None:
            self.ped_indicies = []
            for i in range(len(data.name)):
                if re.search('ped.+', data.name[i]):
                    self.ped_indicies.append(copy.deepcopy(i))
                if re.search('polaris', data.name[i]):
                    self.car_index = copy.deepcopy(i)

        else:
            car_x = data.pose[self.car_index].position.x
            car_y = data.pose[self.car_index].position.y
            car_vx = data.twist[self.car_index].linear.x
            car_vy = data.twist[self.car_index].linear.y
            car_qx = data.pose[self.car_index].orientation.x
            car_qy = data.pose[self.car_index].orientation.y
            car_qz = data.pose[self.car_index].orientation.z
            car_qw = data.pose[self.car_index].orientation.w

            current_dp.append([car_x, car_y])

            for i in range(len(self.ped_indicies)):

                idx = self.ped_indicies[i]
                ped_x = data.pose[idx].position.x
                ped_y = data.pose[idx].position.y
                current_dp.append([i, ped_x, ped_y])

            current_dp.append([car_vx, car_vy])
            current_dp.append([car_qx, car_qy, car_qz, car_qw])
            current_dp.append([timestamp])
            current_dp.append([steering_command])


            collected_data.append(copy.deepcopy(current_dp))

            pickle.dump(collected_data, open("data.pkl", "wb"))
            time.sleep(0.1)




if __name__ == '__main__':

    node = state_parser(ped_goals=[[2,-7.5]])
    rospy.init_node('state_parser', anonymous=True)
    rospy.Subscriber("/gazebo/model_states", ModelStates, node.model_state_cb, queue_size=1)
    rospy.Subscriber("/clock", Clock, clock_cb, queue_size=1)
    rospy.Subscriber("/polaris/front_left_steering_position_controller/command", Float64, steering_command_cb, queue_size=1)
    rospy.spin()
