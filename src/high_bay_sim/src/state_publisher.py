#!/usr/bin/env python

import rospy
import copy
import re
import time
import numpy as np
import math


from std_msgs.msg import Int32, Float64, String, Float64MultiArray
from geometry_msgs.msg import Twist, Pose, Point
from gazebo_msgs.srv import GetModelState, GetModelStateResponse
from gazebo_msgs.msg import ModelStates, LinkStates, LinkState, ModelState
from decawave.msg import position
from decawave.msg import trajectory
from rosgraph_msgs.msg import Clock

pedestrian_state_pub = rospy.Publisher('pedestrian_state', Float64MultiArray, queue_size=1)
pedestrian_pred_traj_pub = rospy.Publisher('predicted_trajectory', trajectory, queue_size=1)
car_state_pub = rospy.Publisher('car_state', Float64MultiArray, queue_size=1)

global timestamp, start_time, epoch_start_time
timestamp = 0
start_time = None
epoch_start_time = time.time()*1000

# Message formats:
# pedestrian_state: [pedestrian ID, timestamp, pedestrian_x, pedestrian_y]
# predicted_trajectory: [pedID, x values], [pedID, y values]
# car_state: [car_x, car_y, heading]



def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


class state_publisher:

    def __init__(self, ped_goals, ped_start_times, ped_end_times, ped_start_pos, ped_end_pos, dt):
        self.ped_indicies = None
        self.car_index = None
        self.ped_goals = ped_goals
        self.predicted_trajs = []
        self.dt = dt

        # Calculate the predicted pedestrian trajectory
        for i in range(len(ped_goals)):
            start_time = ped_start_times[i]
            end_time = ped_end_times[i]
            start_x = ped_start_pos[i][0]
            start_y = ped_start_pos[i][1]
            end_x = ped_end_pos[i][0]
            end_y = ped_end_pos[i][1]
            pred_x = []
            pred_y = []
            for j in range(int(start_time/dt)):
                pred_x.append(start_x)
                pred_y.append(start_y)
            pred_x = np.insert(pred_x, len(pred_x), np.linspace(start_x, end_x, (end_time-start_time)/dt))
            pred_y = np.insert(pred_y, len(pred_y), np.linspace(start_y, end_y, (end_time-start_time)/dt))

            self.predicted_trajs.append([pred_x, pred_y])

    def model_state_cb(self, data):
        if self.ped_indicies == None or self.car_index == None:
            self.ped_indicies = []
            for i in range(len(data.name)):
                if re.search('ped.+', data.name[i]):
                    self.ped_indicies.append(copy.deepcopy(i))
                if re.search('polaris', data.name[i]):
                    self.car_index = copy.deepcopy(i)

        else:
            for i in range(len(self.ped_indicies)):
                global timestamp, start_time

                idx = self.ped_indicies[i]
                ped_x = data.pose[idx].position.x
                ped_y = data.pose[idx].position.y

                # Publish the pedestrian's state
                ped_state = Float64MultiArray()
                ped_state.data = [i, timestamp, ped_x, ped_y]
                pedestrian_state_pub.publish(ped_state)

                # Publish the predicted trajectory
                if start_time != None:
                    idx = int((timestamp-start_time)/self.dt)
                    if idx >= len(self.predicted_trajs[i][0]):
                        idx = len(self.predicted_trajs[i][0]) - 1
                else:
                    idx = 0

                pred_traj = trajectory()
                pred_traj.time = copy.deepcopy(timestamp)*1000 + copy.deepcopy(epoch_start_time)  #Epoch time in milliseconds
                pred_traj.x = self.predicted_trajs[i][0][idx:]
                pred_traj.y = self.predicted_trajs[i][1][idx:]
                pred_traj.x = np.insert(pred_traj.x, 0, i)
                pred_traj.y = np.insert(pred_traj.y, 0, i)
                pedestrian_pred_traj_pub.publish(pred_traj)


            # Publish the car's state
            idx = self.car_index
            car_x = data.pose[idx].position.x
            car_y = data.pose[idx].position.y
    
            raw_heading = self.quaternion_to_euler(data.pose[idx].orientation.x, \
                data.pose[idx].orientation.y, data.pose[idx].orientation.z, data.pose[idx].orientation.w)
            heading = raw_heading[2]
            #heading = math.atan2(car_y, car_x) - math.pi/2
            car_state = Float64MultiArray()
            car_state.data = [car_x, car_y, heading]
            car_state_pub.publish(car_state)
            

    def clock_cb(self, data):
        global timestamp, start_time, epoch_start_time
        timestamp = data.clock.secs + data.clock.nsecs/1000000000.
        if start_time == None:
            start_time = timestamp


    def quaternion_to_euler(self, x, y, z, w):
        x, y, z, w = float(x), float(y), float(z), float(w)

        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)
        roll = math.atan2(t0, t1)
        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2
        pitch = math.asin(t2)
        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)
        yaw = math.atan2(t3, t4)
        return [roll, pitch, yaw]


if __name__ == '__main__':

    # node = state_publisher(ped_goals=[[-5,-7.5], [2,-2]], 
    #                        ped_start_times=[33.,24.], 
    #                        ped_end_times=[47.,36.], 
    #                        ped_start_pos=[[7.5,-7.5],[8.5,-7.]], 
    #                        ped_end_pos=[[-5.,-7.5],[2.,-2.]],
    #                        dt=0.1)
    node = state_publisher(ped_goals=[[9.,-7.]], 
                           ped_start_times=[15.], 
                           ped_end_times=[35.], 
                           ped_start_pos=[[9.,7.]], 
                           ped_end_pos=[[9.,-7.]],
                           dt=0.1)
    # node = state_publisher(ped_goals=[[-20,0], [30, 5]], 
    #                        ped_start_times=[45., 50.], 
    #                        ped_end_times=[65., 70.], 
    #                        ped_start_pos=[[-20.,20.], [20,12]], 
    #                        ped_end_pos=[[-20.,0], [30, 5]],
    #                        dt=0.1)

    rospy.init_node('state_publisher', anonymous=True)
    rospy.Subscriber("/gazebo/model_states", ModelStates, node.model_state_cb, queue_size=1)
    rospy.Subscriber("/clock", Clock, node.clock_cb, queue_size=1)
    rospy.spin()
